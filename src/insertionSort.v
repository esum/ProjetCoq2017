(* begin hide *)
Require Import List.
(* end hide *)

(** Ordre total décidable *)
Module Type DecidableEqLe.

  Parameter A : Type.
  Parameter default : A.

  Parameter le : A -> A -> Prop.
  Infix "<=" := le.
  Parameter le_total : forall a b, a <= b \/ b <= a.
  Parameter leb : A -> A -> bool.
  Infix "<=?" := leb (at level 68).
  Parameter leb_le : forall a b, a <=? b = true <-> a <= b.
  Parameter eqb : A -> A -> bool.
  Infix "=?" := eqb (at level 68).
  Parameter eqb_eq : forall a b, a =? b = true <-> a = b.

End DecidableEqLe.


Module InsertionSort (M : DecidableEqLe).

  Import M.

  Lemma neqb_neq : forall a b, a =? b = false <-> a <> b.
  (* begin hide *)
  Proof.
    intros ; split ; intro.
    - intro.
      apply eqb_eq in H0.
      now rewrite H0 in H.
    - destruct (a =? b) as []eqn:?.
      now apply eqb_eq in Heqb0.
      reflexivity.
  Qed.
  (* end hide *)

  Lemma eqb_refl : forall a, a =? a = true.
  (* begin hide *)
  Proof.
    intro.
    now apply eqb_eq.
  Qed.
  (* end hide *)

  Lemma eqb_sym : forall a b, a =? b = b =? a.
  (* begin hide *)
  Proof.
    intros.
    destruct (b =? a) as []eqn:?.
    - apply eqb_eq in Heqb0.
      subst.
      now rewrite eqb_refl.
    - apply neqb_neq in Heqb0.
      apply neqb_neq.
      intro.
      now apply eq_sym in H.
  Qed.
  (* end hide *)


  Fixpoint insert l n :=
    match l with
    | nil => n::nil
    | m::t => if n <=? m then n::l else m::(insert t n)
    end.

  Fixpoint sort l :=
    match l with
    | nil => nil
    | n::t => insert (sort t) n
    end.

  Fixpoint occ l n :=
    match l with
    | nil => 0
    | m::t => if m =? n then 1 + occ t n else occ t n
    end.


  Definition is_perm l l' := forall a, occ l a = occ l' a.

  Definition is_sorted l := forall n, S n < length l -> nth n l default <= nth (S n) l default.

  Definition is_sort f := forall l, is_perm (f l) l /\ is_sorted (f l).

  Lemma insert_occ : forall l a, occ (insert l a) a = S (occ l a).
  (* begin hide *)
  Proof.
    induction l.
    - intro a.
      simpl.
      now rewrite eqb_refl.
    - intro b.
      simpl.
      destruct (leb b a) ; simpl.
      + destruct (a =? b) ; rewrite eqb_refl ; reflexivity.
      + destruct (a =? b) ; now rewrite IHl.
  Qed.
  (* end hide *)

  Lemma insert_neq_occ : forall l a b, a <> b -> occ (insert l b) a = occ l a.
  (* begin hide *)
  Proof.
    intros.
    induction l ; simpl.
    - apply neqb_neq in H.
      now rewrite eqb_sym, H.
    - apply neqb_neq in H.
      rewrite eqb_sym in H.
      destruct (a0 =? a) as []eqn:? ; destruct (leb b a0) ; simpl ; rewrite Heqb0.
      + now rewrite H.
      + now rewrite IHl.
      + now rewrite H.
      + assumption.
  Qed.
  (* end hide *)

  Lemma insert_length : forall l a, length (insert l a) = S (length l).
  (* begin hide *)
  Proof.
    induction l.
    - easy.
    - intro a0.
      simpl.
      destruct (a0 <=? a).
      + easy.
      + simpl.
        now rewrite IHl.
  Qed.
  (* end hide *)

  Lemma sort_correct_perm : forall l, is_perm (sort l) l.
  Proof.
    induction l.
    - simpl.
      unfold is_perm.
      intro n.
      simpl.
      reflexivity.
    - unfold is_perm.
      intro b.
      simpl.
      destruct (a =? b) as []eqn:?.
      + apply (eqb_eq) in Heqb0.
        now rewrite Heqb0, insert_occ, IHl.
      + apply (neqb_neq a b) in Heqb0.
        rewrite insert_neq_occ.
        apply IHl.
        intro.
        now apply eq_sym in H.
  Qed.

  (** Lemme de stabilité pour l'insertion :
      dans la liste [l' = insert l n], [n] est à la position [m] et
      - [m] ≤ |[l]|
      - Si [m] < |[l]| alors le [(m+1)]#<sup>ème</sup># élément de [l'] est plus grand que [n]
      - ∀[p] ∈ ℕ, [p] < |[l]|
        - si [p] < [m] alors le [p]#<sup>ème</sup># élément de [l'] est égal au [p]#<sup>ème</sup># élémént de [l] et inférieur à [n]
        - si [m] ≤ [p] alors le [(p+1)]#<sup>ème</sup># élément de [l'] est égal au [p]#<sup>ème</sup># élémént de [l] *)
  Lemma insert_le : forall l n, exists m,
    (nth m (insert l n) default = n) (* l'élément est inséré à une position m *)
    /\ (m <= length l)%nat (* la liste au moins m+1 éléments après insertion *)
    /\ (m < length l -> n <= nth (S m) (insert l n) default) (* si le mième n'est pas le dernier élément alors l'élément suivant est plus grand *)
    /\ (forall p, p < length l ->
    (p < m -> 
      (nth p (insert l n) default = nth p l default) (* les éléments aux positions précédant m sont inchangés *)
      /\ (nth p (insert l n) default <= n)) (* les éléments aux positions précédant m sont plus petit que n *)
    /\ ((m <= p)%nat -> nth (S p) (insert l n) default = nth p l default)). (* les élémements aux positions suivant n sont inchangés *)
  Proof.
    (* Toutes les propriétés se préservent par récurrence *)
    induction l.
    - intro n.
      now exists 0.
    - intro n.
      destruct (n <=? a) as []eqn:?.
      + exists 0.
        repeat split.
        * simpl.
          now rewrite Heqb.
        * now rewrite (PeanoNat.Nat.le_0_l (length (a::l))).
        * intros.
          simpl.
          rewrite Heqb.
          simpl.
          now apply leb_le in Heqb.
        * easy.
        * easy.
        * intro.
          simpl.
          rewrite Heqb.
          reflexivity.
      + destruct (IHl n) as [m' IHl'].
        exists (S m').
        repeat split.
        * simpl.
          rewrite Heqb.
          easy.
        * destruct IHl' as [_ [H _]].
          simpl.
          now apply PeanoNat.Nat.succ_le_mono in H.
        * intro.
          simpl.
          rewrite Heqb.
          simpl.
          destruct IHl' as [_ [_ [H' _]]].
          now apply PeanoNat.Nat.succ_lt_mono, H' in H.
        * simpl.
          rewrite Heqb.
          simpl.
          destruct p.
          reflexivity.
          destruct IHl' as [_ [_ [H']]].
          pose proof (H1 p) as H1'.
          apply PeanoNat.Nat.succ_lt_mono, H1' in H.
          destruct H as [H _].
          apply PeanoNat.Nat.succ_lt_mono, H in H0.
          now destruct H0 as [H0 _].
        * simpl.
          rewrite Heqb.
          simpl.
          destruct p.
          destruct (le_total a n) as [Hle|Hle].
          easy.
          apply leb_le in Hle.
          now rewrite Hle in Heqb.
          destruct IHl' as [_ [_ [_ H']]].
          apply PeanoNat.Nat.succ_lt_mono, (H' p) in H.
          destruct H as [H _].
          apply PeanoNat.Nat.succ_lt_mono, H in H0.
          now destruct H0 as [_ H0].
        * intro.
          simpl.
          rewrite Heqb.
          simpl.
          destruct p.
          easy.
          apply PeanoNat.Nat.succ_lt_mono in H.
          apply PeanoNat.Nat.succ_le_mono in H0.
          destruct (IHl') as [_ [_ [_ H1]]].
          pose proof (H1 p) as H1.
          apply H1 in H.
          destruct H as [_ H].
          now apply H in H0.
  Qed.

  Lemma sort_correct_sorted : forall l, is_sorted (sort l).
  Proof.
    induction l.
    - simpl.
      unfold is_sorted.
      now intros.
    - simpl.
      unfold is_sorted.
      intros.
      destruct (insert_le (sort l) a) as [m Hinsert].
      destruct Hinsert as [Ha_in [Hm_in [Hm_next Horder]]].
      rewrite (insert_length (sort l) a) in H.
      apply PeanoNat.Nat.succ_lt_mono in H as H'.
      apply PeanoNat.Nat.succ_lt_mono in H.
      destruct (PeanoNat.Nat.lt_trichotomy n m) as [H0|[H0|H0]].
      + apply (Horder n) in H.
        destruct H as [H _].
        apply H in H0 as H1.
        destruct H1 as [H1 _].
        apply PeanoNat.Nat.le_succ_l, PeanoNat.Nat.le_lteq in H0.
        destruct H0 as [|].
        * apply (PeanoNat.Nat.lt_le_trans (S n) _ (length (sort l))) in H0 as H2.
          apply Horder in H2 as H3.
          destruct H3 as [H3 _].
          apply H3 in H0 as H4.
          clear H3.
          destruct H4 as [H4 _].
          rewrite H4, H1.
          now apply IHl.
          assumption.
        * rewrite H0, Ha_in.
          assert (n < m).
          unfold lt.
          rewrite H0.
          apply Peano.le_n.
          now apply (Horder n) in H2.
      + rewrite H0 in H, H'.
        apply Hm_next in H.
        now rewrite H0, Ha_in.
      + apply (Horder n) in H.
        destruct H as [_ H].
        apply PeanoNat.Nat.lt_le_incl in H0 as H1.
        apply H in H1.
        apply PeanoNat.Nat.lt_le_pred in H0 as H2.
        pose proof (PeanoNat.Nat.le_pred_l n) as H3.
        apply (PeanoNat.Nat.le_lt_trans _ _ (length (sort l))) in H3.
        apply Horder in H3.
        destruct H3 as [_ H3].
        apply H3 in H2.
        clear H3.
        apply (PeanoNat.Nat.le_lt_trans 0 _ _) in H0.
        * apply PeanoNat.Nat.lt_neq, PeanoNat.Nat.neq_sym in H0.
          apply PeanoNat.Nat.succ_pred in H0 as H3.
          rewrite H3 in H2.
          rewrite H1, H2.
          rewrite <- H3 at 2.
          apply IHl.
          now rewrite H3.
        * now apply PeanoNat.Nat.le_0_l.
        * assumption.
  Qed.

  (** Le tri par insertion trie *)
  Theorem insertion_sort : is_sort sort.
  (* begin hide *)
  Proof.
    intro l.
    split.
    - apply sort_correct_perm.
    - apply sort_correct_sorted.
  Qed.
  (* end hide *)

End InsertionSort.


Module NatDecidableEqLe <: DecidableEqLe.

  Definition A := nat.
  Definition le := Peano.le.
  Definition le_total := PeanoNat.Nat.le_ge_cases.
  Definition leb := PeanoNat.Nat.leb.
  Definition leb_le := PeanoNat.Nat.leb_le.
  Definition eqb := PeanoNat.Nat.eqb.
  Definition eqb_eq := PeanoNat.Nat.eqb_eq.
  Definition default := 0.

End NatDecidableEqLe.

(** Les listes d'entiers sont triables pour [<=] par le tri par insertion *)
Module NatInsertionSort := InsertionSort NatDecidableEqLe.
