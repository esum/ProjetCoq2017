Require Import List.

Lemma well_founded_nat : well_founded lt.
Proof.
  unfold well_founded.
  intro n.
  induction n.
  apply Acc_intro.
  intro m.
  intro.
  easy.
  apply Acc_intro.
  intro m.
  intro.
  apply Acc_intro.
  intro p.
  intro.
  inversion IHn.
  unfold lt in H.
  unfold lt in H0.
  unfold lt in H1.
  apply le_n_S in H0.
  apply (PeanoNat.Nat.le_trans _ _ (S n)) in H0 ; try assumption.
  apply le_S_n in H0.
  now apply (H1 p) in H0.
Qed.

Definition lt_product u v :=
  match u, v with
  | (n, n'), (m, m') => n < m /\ n' < m'
  end.

Lemma well_founded_product : well_founded lt_product.
Proof.
  unfold well_founded.
  destruct a as [n n'].
  revert n'.
  induction n.
  intro n'.
  apply Acc_intro.
  destruct y as [m m'].
  intro.
  now destruct H.
  intro n'.
  apply Acc_intro.
  destruct y as [m m'].
  intro.
  destruct H.
  pose proof (IHn (S m')).
  inversion H1.
  Search "succ".
  apply PeanoNat.Nat.le_succ_r in H.
  destruct H.
  apply H2.
  simpl.
  split.
  assumption.
  apply PeanoNat.Nat.lt_succ_diag_r.
  injection H ; intro.
  subst.
  apply IHn.
Qed.

Inductive lt_dict : list nat -> list nat -> Prop :=
  | lt_dict_nil : forall a l, lt_dict nil (a::l)
  | lt_dict_le_cons : forall a a' l l', a <= a' -> lt_dict l l' -> lt_dict (a::l) (a'::l')
  | lt_dict_lt_cons : forall a a' l, a < a' -> lt_dict (a::l) (a'::l).

Fact lt_dict_le_length : forall l l', lt_dict l l' -> length l <= length l'.
Proof.
  intros l l'.
  revert l.
  induction l' ; intros.
  now inversion H.
  induction l.
  apply le_0_n.
  inversion_clear H.
  simpl.
  apply le_n_S.
  now apply IHl'.
  constructor.
Qed.

Lemma well_founded_dict : well_founded lt_dict.
Proof.
  unfold well_founded.
  induction a as [|a l].
  apply Acc_intro.
  intros.
  now inversion H.
  induction IHl as [l IHl1 IHl2].
  apply Acc_intro.
  intros l' ?.
  inversion_clear H.
  apply Acc_intro.
  intros.
  now inversion H.
  destruct (Lt.le_lt_or_eq _ _ H0).
  apply IHl2 in H1.
  apply H1.
  now apply lt_dict_lt_cons.
  subst.
  now apply IHl2.
  apply Acc_intro.
  intros.
  inversion_clear H.
  admit.
  admit.

Inductive lt_lex : list nat -> list nat -> Prop :=
  | lt_lex_length : forall l l', length l < length l' -> lt_lex l l'
  | lt_lex_fst : forall a a' l l', length l = length l' -> a < a'-> lt_lex (a::l) (a'::l')
  | lt_lex_fsteq : forall a l l', length l = length l' -> lt_lex l l' -> lt_lex (a::l) (a::l').

Lemma well_founded_lexic : well_founded lt_lex.
Proof.
  unfold well_founded.
  induction a as [|a l].
  apply Acc_intro.
  intros.
  now inversion H.
  induction a.
  apply Acc_intro.
  induction IHl as [l IHl1 IHl2].
  induction y as [|a' l'].
  intros.
  apply Acc_intro.
  intros.
  now inversion H0.
  intros.
  inversion_clear H.
  simpl in H0.
  apply IHl2 with l'.
  apply lt_lex_length.
  now apply PeanoNat.Nat.succ_lt_mono.
  Search "_ < _".
  induction l'.
  induction
  intros l'.
  induction 1.
  admit.
  now inversion H.
  induction IHl.
  apply Acc_intro.
  intros.
  inversion_clear H1.
  induction y.
  apply Acc_intro.
  intros.
  now inversion H2.
  inversion_clear H1.
  assert (lt_lex y x).
  apply lt_lex_length.
  now apply PeanoNat.Nat.succ_lt_mono.
  apply H0 in H1.
  apply H1.
  apply H1.
  
  intros l' ?.
  induction H.
  apply 
  remember (length l).
  induction n.
  destruct l.
  apply Acc_intro.
  intros.
  now inversion H.
  easy.
  induction a as [|n l] ; apply Acc_intro.
  intros l' H.
  now inversion H.
  induction n.
  intros l' H.
  inversion_clear H.
  assert (length l' < length l \/ length l' = length l) by admit.
  destruct H.
  apply IHl.
  now constructor.
  admit.
  induction l'.
  admit.
  inversion_clear H.
  assert (length (a::l') < length l \/ length (a::l') = length l) by admit.
  destruct H.
  apply IHl.
  now apply lt_lex_length.
Abort.
