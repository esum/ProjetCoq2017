Definition PeirceLaw := forall A B : Prop, ((A -> B) -> A) -> A.

Definition DoubleNegElim := forall A, ~~A -> A.

Definition ExcludedMiddle := forall A, A \/ ~A.

Definition ClassicImpl := forall A B : Prop, (A -> B) -> (~A \/ B).

(** Les 3 autres lois de De Morgan sont vraies en logique intuitionniste *)
Definition DeMorgan := forall A B, ~ (A /\ B) -> ~ A \/ ~ B.
Definition StrongDeMorgan := forall A B, ~(~A /\ ~B) -> A \/ B.


(** Définitions avec le mot clé [Axiom] *)
Module ClassicAxioms.
  Axiom peirce_law : PeirceLaw.
  Axiom double_neg_elim : DoubleNegElim.
  Axiom excluded_middle : ExcludedMiddle.
  Axiom classic_impl : ClassicImpl.
  Axiom string_de_morgan : StrongDeMorgan.
End ClassicAxioms.


(** * Équivalences
    On montre les équivalences :
    - loi de Peirce ⇔ tiers exclus
    - élémination de la double négation ⇔ tiers exclus
    - implication classique ⇔ tiers exclus
    - loi de De Morgan forte ⇔ élémination de la double négation
    D'où l'équivalence entre les 5 propositions. *)

Lemma PeirceLaw_yields_DoubleNegElim : PeirceLaw -> DoubleNegElim.
Proof.
  intros Ax A H.
  now apply Ax with False.
Qed.

Lemma DoubleNegElim_yields_ExcludedMiddle : DoubleNegElim -> ExcludedMiddle.
Proof.
  intros Ax A.
  apply (Ax (A \/ ~A)).
  intro H.
  cut (~A /\ ~~A).
  - intro H0.
    now destruct H0.
  - split ; intro ; apply H.
    + now left.
    + now right.
Qed.

(* begin show *)
Lemma PeirceLaw_ExcludedMiddle : PeirceLaw <-> ExcludedMiddle.
Proof.
  split.
  - intros Ax A.
    now apply PeirceLaw_yields_DoubleNegElim, DoubleNegElim_yields_ExcludedMiddle in Ax.
  - intros Ax A B H.
    destruct (Ax A).
    + easy.
    + now apply H.
Qed.
(* end show *)

(* begin show *)
Lemma DoubleNegElim_ExcludedMiddle : DoubleNegElim <-> ExcludedMiddle.
Proof.
  split.
  - intro.
    now apply DoubleNegElim_yields_ExcludedMiddle.
  - intros Ax A H.
    destruct Ax with A.
    + assumption.
    + now elim H.
Qed.
(* end show *)

(* begin show *)
Lemma ClassicImpl_ExcludedMiddle : ClassicImpl <-> ExcludedMiddle.
Proof.
  split.
  - intros Ax A.
    now apply or_comm, Ax.
  - intros Ax A B H.
    destruct (Ax A) as [H0|H0].
    + right.
      now apply H.
    + now left.
Qed.
(* end show *)

(* begin show *)
Lemma StrongDeMorgan_DoubleNegElim : StrongDeMorgan <-> DoubleNegElim.
Proof.
  split.
  - intros Ax A H.
    assert (~(~A /\ ~False)) as H0.
      intro.
      now apply H.
    apply Ax in H0.
    now destruct H0.
  - intros Ax A B H.
    apply -> DoubleNegElim_ExcludedMiddle in Ax.
    destruct (Ax A) as [H0|H0] ; [| destruct (Ax B) as [H1|H1]].
    + now left.
    + now right.
    + now elim H.
Qed.
(* end show *)

(** * Loi de De Morgan 
    On montre que la loi de De Morgan dans sa formulation usuelle est équivalente au tiers exclus faible *)

Definition WeakExcludedMiddle := forall A, ~A \/ ~~A.

(* begin show *)
Lemma DeMorgan_WeakExcludedMiddle : DeMorgan <-> WeakExcludedMiddle.
Proof.
  split.
  - intros Ax A.
    apply Ax.
    intro H.
    now destruct H.
  - intros Ax A B H.
    destruct (Ax A) as [H0|H0] ; [| destruct (Ax B) as [H1|H1]].
    + now left.
    + now right.
    + exfalso.
      apply H0.
      intro.
      apply H1.
      intro.
      apply H.
      now split.
Qed.
(* end show *)