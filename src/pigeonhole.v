(* begin hide *)
Require Import List.
(* end hide *)

(** Appartenance à une liste *)
Inductive in_list {X : Type} : X -> list X -> Prop :=
  | in_cons_l : forall x l, in_list x (x::l)
  | in_cons_r : forall x y l, in_list x l -> in_list x (y::l).
Infix "∈" := in_list (at level 49).

(** Correspondance entre le prédicat inductif [in_list] et sa version itérative qui nécessite un objet de type [X] *)
Lemma in_list_nth {X : Type} (default : X) : forall x l, x ∈ l <-> exists n, n < length l /\ x = nth n l default.
(* begin hide *)
Proof.
  intros.
  split.
  - induction l.
    + intro.
      inversion_clear H.
    + intro.
      inversion H ; subst.
      * exists 0.
        split.
        apply PeanoNat.Nat.lt_0_succ.
        reflexivity.
      * apply IHl in H2.
        destruct H2 as [n [H2 H3]].
        exists (S n).
        split.
        now apply (PeanoNat.Nat.succ_lt_mono n _).
        assumption.
  - induction l.
    + intro.
      now destruct H as [n [H _]].
    + intro.
      destruct H as [n [H H0]].
      destruct n.
      * simpl in H0.
        subst.
        apply in_cons_l.
      * apply in_cons_r.
        apply IHl.
        exists n.
        split.
        now apply PeanoNat.Nat.succ_lt_mono.
        assumption.
Qed.
(* end hide *)

(** Répétition d'une liste *)
Inductive repeats {X : Type} : list X -> Prop :=
  | repeats_cons_l : forall x l, x ∈ l -> repeats (x::l)
  | repeats_cons_r : forall x l, repeats l -> repeats (x::l).

(** Enlève le [n]#<sup>ème</sup> élément d'une liste *)
Fixpoint remove_nth {X : Type} n (l : list X) :=
  match n, l with
  | 0, a::l => l
  | S n, a::l => a::(remove_nth n l)
  | _, nil => nil
  end.

Lemma remove_nth_length {X : Type} : forall n (l : list X), n < length l -> length l = S (length (remove_nth n l)).
(* begin hide *)
Proof.
  intros n l.
  revert n.
  induction l.
  - easy.
  - intros.
    simpl.
    apply PeanoNat.Nat.succ_inj_wd.
    + destruct n.
      * easy.
      * now apply IHl, PeanoNat.Nat.succ_lt_mono.
Qed.
(* end hide *)

Lemma remove_nth_length_le {X : Type} : forall n (l : list X), length l <= S (length (remove_nth n l)).
(* begin hide *)
Proof.
  intros n l.
  revert n.
  induction l as [|x l] ; intros.
  - apply le_0_n.
  - destruct n.
    + easy.
    + apply le_n_S, IHl.
Qed.
(* end hide *)

(** 1#<sup>er</sup># lemme de stabilité par supression d'un élément *)
Lemma remove_nth_left {X : Type} (default : X) : forall n l m, m < n -> nth m (remove_nth n l) default = nth m l default.
(* begin hide *)
Proof.
  intros n l.
  revert n.
  induction l as [|x l] ; intros.
  - now destruct n.
  - induction n.
    + easy.
    + apply PeanoNat.Nat.succ_le_mono, PeanoNat.Nat.le_lteq in H.
      destruct H.
      * rewrite <- IHn ; try assumption.
        destruct m.
        -- now destruct n.
        -- simpl.
           destruct n.
           ++ easy.
           ++ simpl remove_nth at 2.
              simpl nth at 2.
              simpl in IHn.
              rewrite IHn ; try assumption.
              now apply IHl, PeanoNat.Nat.lt_le_incl.
      * subst.
        destruct n.
        -- reflexivity.
        -- assert (n < S n) by constructor.
           now apply IHl in H.
Qed.
(* end hide *)


(** 2#<sup>ème</sup> lemme de stabilité par supression d'un élément *)
Lemma remove_nth_right {X : Type} (default : X) : forall n l m, n <= m -> nth m (remove_nth n l) default = nth (S m) l default.
(* begin hide *)
Proof.
  intros n l.
  revert n.
  induction l as [|x l] ; intros.
  - now destruct n ; destruct m.
  - simpl.
    destruct m.
    + apply Le.le_n_0_eq in H.
      now subst.
    + destruct n.
      * reflexivity.
      * now apply IHl, PeanoNat.Nat.succ_le_mono.
Qed.
(* end hide *)

Definition in_list_decidable X := forall (x : X) l, x ∈ l \/ ~ x ∈ l.

(* begin show *)
(** Théorème des tiroirs en supposant que l'on peut décider si un élément de type [X] appartient à une liste de type [list X] *)
Theorem pigeonhole_classic {X : Type} : in_list_decidable X -> 
  forall l1 l2 : list X, length l2 < length l1 -> 
  (forall x, x ∈ l1 -> x ∈ l2) ->
  repeats l1.
Proof.
  intro H1.
  induction l1.
  - easy.
  - intros.
    pose proof (H1 a l1) as H1.
    (* On applique l'hypothèse de décidabilité *)
    destruct H1.
    + now constructor.
    + apply repeats_cons_r.
      assert (a ∈ l2) by (apply H0 ; constructor).
      apply (in_list_nth a) in H2.
      destruct H2 as [n H2].
      apply (IHl1 (remove_nth n l2)). 
      (* On applique l'hypothèse de récurrence sur l2 privée de son nième élément,
         cela fonctionne car on a supposé que a n'était pas dans l1 *)
      * unfold lt.
        destruct H2 as [H2 _].
        apply remove_nth_length in H2.
        rewrite <- H2.
        now apply Lt.lt_n_Sm_le.
      * intros.
        assert (x ∈ (a::l1)) by (now apply in_cons_r).
        apply H0, (in_list_nth a) in H4.
        destruct H4 as [m H4].
        (* On compare n et m en pour utiliser les lemmes stabilité *)
        destruct (PeanoNat.Nat.lt_trichotomy m n) as [|[|]].
        -- apply (in_list_nth a).
           exists m.
           split.
           ++ destruct H2.
              apply (PeanoNat.Nat.le_lt_trans (S m) _ (length l2)) in H5 ; try assumption.
              pose proof (remove_nth_length_le n l2).
              now apply PeanoNat.Nat.succ_lt_mono, PeanoNat.Nat.lt_le_trans with (length l2).
           ++ destruct H4.
              subst.
              now apply eq_sym, remove_nth_left.
           (* Cas absurde *)
        -- subst.
           elim H1.
           destruct H2.
           destruct H4.
           now rewrite H5, <- H6.
           (* On détruit m pour appliquer le 2ème lemme de stabilité sur (S m) *)
        -- destruct m. 
           ++ easy.
           ++ apply (in_list_nth a).
              exists m.
              split ; destruct H4.
              ** pose proof (remove_nth_length_le n l2).
                 apply (PeanoNat.Nat.lt_le_trans _ _ (S (length (remove_nth n l2)))) in H4 ; try assumption.
                 now apply PeanoNat.Nat.succ_lt_mono.
              ** subst.
                 now apply eq_sym, remove_nth_right, PeanoNat.Nat.succ_le_mono.
Qed.
(* end show *)

(* begin show *)
(** Plongement de [l1] dans une liste d'entiers [l3] vérifiant :
    - |[l3]| = |[l1]|
    - ∀[n] ∈ [l3], [n] < |[l2]|
    - Le [n]#<sup>ème</sup># élément de [l3] donne la position de l'élément de [l2] égal au [n]#<sup>ème</sup># élément de [l1]
    - Si [l3] se répète alors [l1] se répète *)
Lemma pigeonhole_aux {X : Type} (default : X) : forall l1 l2 : list X,
  (forall x, x ∈ l1 -> x ∈ l2) ->
  exists l3 : list nat, 
    length l3 = length l1 /\ 
    (forall n, n < length l3 -> nth n l3 0 < length l2) /\
    (forall n, n < length l3 -> nth (nth n l3 0) l2 default = nth n l1 default) /\
    (repeats l3 -> repeats l1).
Proof.
  induction l1.
  - intros.
    now exists nil.
  - intros.
    destruct (IHl1 l2) as [l3 ?].
    + intros.
      apply H.
      now constructor.
    + assert (a ∈ (a::l1)) by constructor.
      apply H, (in_list_nth default) in H1.
      destruct H1 as [n []].
      exists (n::l3).
      destruct H0 as [?[?[]]].
      (* On prouve d'abord ce goal car il permet de prouver la dernière conclusion,
         celle-ci est donc redondante mais est utile dans le théorème des tiroirs *)
      assert ((forall n0 : nat, n0 < length (n::l3) -> nth (nth n0 (n::l3) 0) l2 default = nth n0 (a::l1) default)).
        induction n0 ; intro.
        easy.
        now apply H4, PeanoNat.Nat.succ_lt_mono.
      repeat split.
      * simpl.
        now rewrite H0.
      * destruct n0 ; intro.
        -- assumption.
        -- now apply H3, PeanoNat.Nat.succ_lt_mono.
      (* Déjà prouvé *)
      * assumption.
      * intros.
        inversion_clear H7.
        -- apply repeats_cons_l, (in_list_nth default).
           apply (in_list_nth 0) in H8.
           destruct H8 as [m []].
           pose proof (H6 (S m)).
           apply PeanoNat.Nat.succ_lt_mono in H7 as ?.
           apply H9 in H10.
           simpl in H10.
           exists m.
           split.
           ++ now rewrite <- H0.
           ++ now rewrite <- H10, <- H8.
        -- now apply repeats_cons_r, H5.
Qed.
(* end show *)

(* Énumération des éléments d'une liste en commençant par n *)
Fixpoint enumerate {X : Type} (l : list X) n :=
  match l with
  | nil => nil
  | _::l => n::(enumerate l (S n))
  end.

Lemma enumerate_length {X : Type} : forall (l : list X) n, length (enumerate l n) = length l.
(* begin hide *)
Proof.
  induction l ; intros.
  - reflexivity.
  - simpl.
    cut (length (enumerate l (S n)) = length l).
    intro.
    now rewrite H.
    apply IHl.
Qed.
(* end hide *)

Lemma enumerate_nth {X : Type} : forall n (l : list X) m, n < length l -> nth n (enumerate l m) 0 = n + m.
(* begin hide *)
Proof.
  intros n l m.
  revert n m.
  induction l.
  - now intros.
  - destruct n ; intros.
    + easy.
    + simpl.
      rewrite IHl.
      easy.
      now apply PeanoNat.Nat.succ_lt_mono.
Qed.
(* end hide *)

(* On peut décider si un entier appartient à une liste *)
Lemma in_list_nat_decidable : in_list_decidable nat.
(* begin hide *)
Proof.
  intro n.
  induction l.
  - now right.
  - destruct (PeanoNat.Nat.eq_decidable n a).
    + left.
      subst.
      apply in_cons_l.
    + destruct IHl.
      * left.
        now constructor.
      * right.
        intro.
        apply H0.
        now inversion H1.
Qed.
(* end hide *)

(* begin show *)
(** Théorème des tiroirs intuitionniste *)
Theorem pigeonhole {X : Type} : forall l1 l2 : list X,
  length l2 < length l1 ->
  (forall x, x ∈ l1 -> x ∈ l2) ->
  repeats l1.
Proof.
  intros l1 l2 H H0.
  (* On détruit l1 car cela donne un élément de type X que l'on utilise comme default *)
  destruct l1 as [|x l1']eqn:?.
  (* On suppose length l2 < 0 *)
  - easy.
  - rewrite <- Heql in *.
    clear Heql l1'.
    destruct (pigeonhole_aux x l1 l2) as [l3 [H1 [H2 H3]]].
    + assumption.
      (* Le plongement de l2 est donné par l'énumération de ses éléments.
         On applique le théorème dans sa version classique à l3 et à ce plongement puis on prouve les hypothèses. *)
    + apply H3, (pigeonhole_classic in_list_nat_decidable) with (enumerate l2 0) ; clear H3 x.
      * now rewrite enumerate_length, H1.
      * intros m ?.
        apply (in_list_nth 0) in H3.
        apply (in_list_nth 0).
        destruct H3 as [n []].
        rewrite H4.
        clear H4 m.
        exists (nth n l3 0).
        split.
        -- rewrite enumerate_length.
           now apply H2.
        -- rewrite <- (PeanoNat.Nat.add_0_r _) at 1.
           now apply eq_sym, enumerate_nth, H2.
Qed.
(* end show *)