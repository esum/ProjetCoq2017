(** printing <=° #≤<sub>𝕆</sub># *)
(** printing <° #<<sub>𝕆</sub># *)
(** printing =° #=<sub>𝕆</sub># *)
(** printing <>° #≠<sub>𝕆</sub># *)
(** printing +° #+<sub>𝕆</sub># *)
(** printing -° #-<sub>𝕆</sub># *)
(** printing *° #×<sub>𝕆</sub># *)
(** printing ^° #^<sub>𝕆</sub># *)
(** printing Zero #0# *)
(** printing zero #0# *)
(** printing ω^ #ω<sup># *)
(** printing +: #</sup>+# *)
(** printing =? #=<sub>?</sub># *)
(** printing <=? #≤<sub>?</sub># *)
(** printing <? #<<sub>?</sub># *)

(* begin hide *)
Require Setoid.
(* end hide *)

(* Relation d'ordre partielle pour une relation d'équivalence donnée *)
Definition partial_order {A : Type} (equal : A -> A -> Prop) (R : A -> A -> Prop) :=
  (forall a, R a a)
  /\ (forall a b c, R a b -> R b c -> R a c)
  /\ (forall a b, R a b -> R b a -> equal a b).

(* begin hide *)
Module Ord.
(* end hide *)

(** Représentation des ordinaux.
    [Limit o] représente sup#<sub>n ∈ ℕ</sub> o(n) *)
Inductive Ord : Set :=
  | Zero : Ord
  | Succ : Ord -> Ord
  | Limit : (nat -> Ord) -> Ord.

(** Un ordre partiel de prédécesseur *)
Fixpoint pred_ord a b : Prop :=
  match b with
  | Zero => False
  | Succ b' => a = b' \/ pred_ord a b'
  | Limit o => exists n, pred_ord a (o n)
  end.
(** On peut voir un objet du type [Ord] comme un arbre avec des feuilles ([Zero]), des noeuds d'arité 1 ([Succ]) et des noeuds d'arité ℵ#<sub>0</sub># ([Limit]).
   [pred_ord a b] signifie que [a] est un sous-arbre strict de [b] avec la condition que son père dans [b] soit un noeud [Succ] *)
(* begin hide *)
(* end hide *)

(** Relation d'ordre sur [Ord] *)
Inductive le_ord : Ord -> Ord -> Prop :=
  | le_ord_Zero : forall a, le_ord Zero a
  | le_ord_Succ : forall a b c, le_ord a c -> pred_ord c b -> le_ord (Succ a) b
  | le_ord_Limit : forall o a, (forall n, le_ord (o n) a) -> le_ord (Limit o) a.
Infix "<=°" := le_ord (at level 70).

(** Version stricte de [<=°] *)
Definition lt_ord a b := Succ a <=° b.
Infix "<°" := lt_ord (at level 70).

(** égalité sur [Ord] *)
Definition eq_ord a b := a <=° b /\ b <=° a.
Infix "=°" := eq_ord (at level 70).
Notation "a <>° b" := (~(a =° b)) (at level 70).

(** Addition sur [Ord] *)
Fixpoint add_ord a b :=
  match b with
  | Zero => a
  | Succ c =>  Succ (add_ord a c)
  | Limit o => Limit (fun n => add_ord a (o n))
  end.
Infix "+°" := add_ord (at level 50).

(** Multiplication sur [Ord] *)
Fixpoint mul_ord a b :=
  match b with
  | Zero => Zero
  | Succ c => add_ord (mul_ord a c) a
  | Limit o => Limit (fun n => mul_ord a (o n))
  end.
Infix "*°" := mul_ord (at level 40).

(** Exponentiation sur [Ord] *)
Fixpoint pow_ord a b :=
  match b with
  | Zero => Succ Zero
  | Succ c => mul_ord (pow_ord a c) a
  | Limit o => Limit (fun n => pow_ord a (o n))
  end.
Infix "^°" := pow_ord (at level 30).

Lemma nle_ord_zero_succ : forall a, ~ Succ a <=° Zero.
Proof. 
  intros a H.
  inversion H.
  now simpl in H1.
Qed.

Lemma le_ord_zero_r : forall a b, a <=° Zero -> a <=° b.
Proof.
  induction a ; intros.
  - constructor.
  - inversion_clear H.
    now simpl in H0.
  - inversion_clear H0.
    apply (le_ord_Limit o b).
    intro.
    apply (H n b), H1.
Qed.

Lemma le_ord_succ_l : forall a b, Succ a <=° b -> a <=° b.
Proof.
  induction a ; intros.
  - constructor.
  - inversion_clear H.
    apply IHa in H0.
    now apply le_ord_Succ with c.
  - inversion_clear H0.
    inversion_clear H1.
    apply (le_ord_Limit o b).
    intro.
    pose proof (H0 n) as H0.
    apply (le_ord_Succ _ b _) in H0.
    now apply (H n b).
    assumption.
Qed.

Lemma le_ord_succ_r : forall a b, a <=° b -> a <=° Succ b.
Proof.
  induction a ; intros.
  - constructor.
  - apply le_ord_succ_l in H.
    assert (pred_ord b (Succ b)) by (now left).
    now apply le_ord_Succ with b.
  - inversion_clear H0.
    apply le_ord_Limit.
    intro n.
    pose proof (H1 n) as H1.
    now apply (H n b) in H1.
Qed.

Lemma le_ord_pred_mono : forall a b, Succ a <=° Succ b -> a <=° b.
Proof.
  inversion_clear 1.
  destruct H1.
  - now rewrite <- H.
  - apply (le_ord_Succ _ b _) in H0.
    now apply (le_ord_succ_l a b) in H0.
    assumption.
Qed.

Lemma le_ord_limit_r : forall a o n, a <=° o n -> a <=° Limit o.
Proof.
  induction a ; intros.
  - constructor.
  - destruct  (o n) as []eqn:?.
    + now pose proof (nle_ord_zero_succ a).
    + apply (le_ord_pred_mono a o0) in H as H'.
      assert (pred_ord o0 (Limit o)).
        simpl.
        exists n.
        rewrite Heqo0.
        simpl.
        now left.
      now apply le_ord_Succ with o0.
    + inversion_clear H.
      assert (pred_ord c (Limit o)) by (exists n ; now rewrite Heqo0).
      now apply le_ord_Succ with c.
  - inversion_clear H0.
    apply le_ord_Limit.
    intro.
    pose proof (H1 n0) as H1.
    now apply H in H1.
Qed.

Lemma le_ord_succ_mono : forall a b, a <=° b -> Succ a <=° Succ b.
Proof.
  intros.
  apply (le_ord_Succ a (Succ b) b) in H.
  easy.
  now left.
Qed.

Lemma nle_ord_succ : forall a, ~ Succ a <=° a.
Proof.
  induction a ; intro.
  - now apply (nle_ord_zero_succ Zero).
  - now apply IHa, le_ord_pred_mono.
  - inversion_clear H0.
    inversion_clear H1.
    destruct H2 as [n H2].
    apply (H n).
    now apply le_ord_Succ with c.
Qed.

Lemma le_ord_pred_l : forall a b c, a <=° b -> pred_ord c a -> c <=° b.
Proof.
  induction a ; intros.
  - contradiction.
  - apply (le_ord_succ_l a b) in H.
    destruct H0.
    + now rewrite <- H0 in H.
    + now apply (IHa b c) in H.
  - inversion_clear H0.
    destruct H1 as [n H1].
    pose proof (H2 n) as H2.
    now apply (H n b c) in H2.
Qed.

Lemma le_ord_pred_r : forall a b c, a <=° c -> pred_ord c b -> a <=° b.
Proof.
  intros.
  apply (le_ord_Succ a b c) in H.
  now apply (le_ord_succ_l a b) in H.
  assumption.
Qed.

(* begin show *)
(** Réflexivité de [<=°] *)
Lemma le_ord_refl : forall a, a <=° a.
Proof.
  induction a.
  - constructor.
  - now apply (le_ord_succ_mono a a) in IHa.
  - apply (le_ord_Limit o (Limit o)).
    intro n.
    pose proof (H n) as H.
    now apply (le_ord_limit_r (o n) o n) in H.
Qed.
(* end show *)

(* begin show *)
(** Trantivité de [<=°] *)
Lemma le_ord_trans : forall a b c, a <=° b -> b <=° c -> a <=° c.
Proof.
  intros a b c H.
  revert c.
  induction H ; intros.
  - constructor.
  - induction H1.
    + contradiction.
    + destruct H0.
      * rewrite <- H0 in H1.
        now apply (IHle_ord c0), (le_ord_Succ a b c0) in H1.
      * now apply IHle_ord0, (le_ord_Succ (Succ a) b c0), (le_ord_succ_l (Succ a) b) in H0.
    + destruct H0.
      now apply (H2 x) in H0.
  - constructor.
    intro.
    now apply (H0 n c) in H1.
Qed.
(* end show *)

(** [<=°] est un ordre partiel *)
Theorem le_ord_partial_order : partial_order eq_ord le_ord.
Proof.
  split ; try split.
  - apply le_ord_refl.
  - apply le_ord_trans.
  - now intros.
Qed.

(** On peut prouver que [<=°] est un ordre total en logique classique *)
Theorem le_ord_total_classic : (forall P, P \/ ~P) -> forall a b, a <=° b \/ b <=° a.
Proof.
  intro LEM.
  induction a.
  - left.
    constructor.
  - induction b.
    + right.
      constructor.
    + destruct (IHa b).
      * left.
        now apply le_ord_succ_mono.
      * right.
        now apply le_ord_succ_mono.
    + assert ((exists n, Succ a <=° o n) \/ forall n, o n <=° Succ a).
        destruct (LEM (forall n, o n <=° Succ a)).
        now right.
        destruct (LEM (exists n, Succ a <=° o n)).
        now left.
        elim H0.
        intro.
        destruct (H n).
        elim H1.
        now exists n.
        assumption.
      destruct H0.
      * left.
        destruct H0 as [n H0].
        now apply le_ord_limit_r with n.
      * right.
        now constructor.
  - intro b.
    assert ((exists n, b <=° o n) \/ forall n, o n <=° b).
      destruct (LEM (forall n, o n <=° b)).
      now right.
      destruct (LEM (exists n, b <=° o n)).
      now left.
      elim H0.
      intro.
      destruct (H n b).
      assumption.
      elim H1.
      now exists n.
      destruct H0.
      right.
      destruct H0 as [n H0].
      now apply le_ord_limit_r with n.
    left.
    now constructor.
Qed.

Lemma eq_eq_ord_inc : forall a b, a = b -> a =° b.
Proof.
  intros.
  rewrite H.
  split ; apply le_ord_refl.
Qed.

Lemma neq_ord_neq_inc : forall a b, a <>° b -> a <> b.
Proof.
  intros.
  intro.
  apply H.
  rewrite H0.
  now apply eq_eq_ord_inc.
Qed.

Lemma nle_ord_pred_r : forall a b c, a <=° b -> pred_ord c a -> ~ b <=° c.
Proof.
  induction a ; intros ; intro.
  - contradiction.
  - apply (nle_ord_succ a), le_ord_trans with b.
    + assumption.
    + now apply le_ord_pred_mono, le_ord_Succ with c.
  - destruct H1 as [n H1].
    inversion_clear H0.
    now apply (H n b c).
Qed.

Lemma lt_ord_antisym : forall a b, a <° b -> ~ b <° a.
Proof.
  intros.
  intro.
  inversion_clear H.
  inversion_clear H0.
  apply (nle_ord_pred_r a b c0).
  - apply le_ord_succ_l.
    now apply le_ord_Succ with c.
  - assumption.
  - assumption.
Qed.

Lemma neq_ord_succ : forall a, ~ a =° Succ a.
Proof.
  intros.
  intro.
  destruct H.
  now pose proof (nle_ord_succ a).
Qed.

Lemma lt_ord_irefl : forall a, ~ a <° a.
Proof.
  intro a.
  intro.
  now apply (nle_ord_succ a).
Qed.

Lemma neq_succ : forall a, ~ a = Succ a.
Proof.
  intro.
  intro.
  apply eq_eq_ord_inc in H.
  now apply (neq_ord_succ a).
Qed.

Lemma le_ord_succ_pred_ord_r : forall a b c, pred_ord c (Succ b) -> a <=° c -> a <=° b.
Proof.
  induction a ; intros.
  - constructor.
  - destruct H.
    + now rewrite <- H.
    + now apply le_ord_succ_l, (le_ord_Succ _ b _) in H0.
  - destruct H0.
    + now rewrite <- H0.
    + inversion_clear H1.
      constructor.
      intro.
      pose proof (H2 n) as H2.
      apply (H n b c).
      now right.
      assumption.
Qed.

Lemma pred_ord_lt : forall a b, pred_ord a b -> a <° b.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - contradiction.
  - destruct H.
    + rewrite H.
      now apply le_ord_refl.
    + now apply le_ord_succ_r, IHb.
  - destruct H0 as [n H0]. 
    now apply le_ord_limit_r with n, H.
Qed.

Lemma lt_ord_le_incl : forall a b, a <° b -> a <=° b.
Proof.
  intros a b H.
  apply le_ord_pred_mono.
  now apply le_ord_succ_r.
Qed.

Lemma le_ord_add : forall a b, a <=° a +° b.
Proof.
  intros.
  revert a.
  induction b ; intros ; simpl.
  - now apply le_ord_refl.
  - now apply le_ord_succ_r.
  - apply le_ord_limit_r with 0, H.
Qed.

Lemma le_ord_add2 : forall a b, a <=° b +° a.
Proof.
  induction a ; intros.
  - constructor.
  - now apply le_ord_succ_mono.
  - apply le_ord_Limit.
    intro.
    now apply le_ord_limit_r with n.
Qed.

Lemma le_ord_add_succ : forall a b, Zero <° b -> Succ a <=° a +° b.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - now elim nle_ord_zero_succ with Zero.
  - apply le_ord_succ_mono, le_ord_add.
  - inversion_clear H0.
    destruct H2 as [n H2].
    apply (le_ord_Succ _ (o n) _) in H1.
    now apply le_ord_limit_r with n, H.
    assumption.
Qed.

Lemma le_ord_add_r : forall a b c, a <=° b -> a +° c <=° b +° c.
Proof.
  intros.
  induction c.
  - easy.
  - now apply le_ord_succ_mono.
  - apply le_ord_Limit.
    intro n.
    now apply le_ord_limit_r with n.
Qed.

Lemma le_ord_add_l : forall a b c, a <=° b -> c +° a <=° c +° b.
Proof.
  induction 1 ; intros.
  - now apply le_ord_add.
  - induction b ; simpl.
    + contradiction.
    + destruct H0.
      * rewrite H0 in IHle_ord.
        now apply le_ord_succ_mono.
      * now apply le_ord_succ_r, IHb.
    + destruct H0 as [n H0].
      now apply le_ord_limit_r with n, H1.
  - now apply le_ord_Limit.
Qed.

Lemma le_ord_add_lr : forall a b c d, a <=° b -> c <=° d -> a +° c <=° b +° d.
Proof.
  intros.
  apply le_ord_trans with (b +° c).
  - now apply le_ord_add_r.
  - now apply le_ord_add_l.
Qed.

Lemma lt_ord_add_lr : forall a b c d, a <=° b -> c <° d -> a +° c <° b +° d.
Proof.
  intros.
  now apply (le_ord_add_lr _ _ (Succ c) _).
Qed.

Lemma ord_add_zero : forall a b, a +° b = Zero -> a = Zero /\ b = Zero.
Proof.
  now destruct b.
Qed.

Lemma eq_ord_add_zero : forall a, a =° Zero +° a.
Proof.
  induction a.
  - split ; apply le_ord_refl.
  - destruct IHa.
    now split ; apply le_ord_succ_mono.
  - split ; constructor ; intro n ; apply le_ord_limit_r with n, H.
Qed.

Lemma le_ord_mul : forall a b, Zero <° b -> a <=° a *° b.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - now pose proof (nle_ord_zero_succ Zero).
  - apply le_ord_add2.
  - inversion_clear H0. 
    destruct H2 as [n H2].
    apply le_ord_limit_r with n, H, le_ord_trans with (Succ c).
    + now apply le_ord_succ_mono.
    + now apply pred_ord_lt.
Qed.

Lemma le_ord_mul2 : forall a b, Zero <° b -> a <=° b *° a.
Proof.
  induction a ; intros.
  - constructor.
  - destruct b.
    + now pose proof (nle_ord_zero_succ Zero).
    + simpl. apply le_ord_succ_mono.
      apply IHa in H.
      pose proof (le_ord_add (Succ b *° a) b).
      now apply (le_ord_trans _ _ (Succ b *° a +° b)) in H.
    + apply IHa in H as H0.
      assert (Succ a = a +° Succ Zero) by easy.
      rewrite H1.
      now apply (le_ord_add_lr _ (Limit o *° a) _ (Limit o)).
  - apply le_ord_Limit.
    intro n.
    now apply le_ord_limit_r with n, H.
Qed.

Lemma le_ord_mul_r : forall a b c, a <=° b -> a *° c <=° b *° c.
Proof.
  induction c ; intros.
  - constructor.
  - simpl.
    apply IHc in H as H0.
    apply (le_ord_add_r (a *° c) (b *° c) a) in H0.
    apply (le_ord_add_l a b (b *° c)) in H.
    now apply le_ord_trans with (b *° c +° a).
  - simpl.
    constructor.
    intro n.
    apply le_ord_limit_r with n.
    now apply H.
Qed.

Lemma le_ord_mul_l : forall a b c, a <=° b -> c *° a <=° c *° b.
Proof.
  induction 1 ; intros.
  - constructor.
  - induction b ; simpl.
    + contradiction.
    + destruct H0.
      * rewrite H0 in IHle_ord.
        now apply (le_ord_add_r (c *° a) (c *° b) c).
      * apply IHb in H0.
        simpl in H0.
        apply le_ord_trans with (c *° b).
        assumption.
        apply le_ord_add.
    + destruct H0 as [n H0].
      pose proof (H1 n) as H1.
      apply H1 in H0.
      simpl in H0.
      now apply le_ord_limit_r with n.
  - now apply le_ord_Limit.
Qed.

Lemma ord_integral : forall a b, a *° b = Zero -> a = Zero \/ b = Zero.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - now right.
  - simpl in H.
    apply ord_add_zero in H.
    destruct H as [_ H].
    now left.
  - easy.
Qed.

Lemma eq_ord_mul_one : forall a, a =° Succ Zero *° a.
Proof.
  induction a.
  - simpl.
    split ; apply le_ord_refl.
  - simpl.
    destruct IHa.
    now split ; apply le_ord_succ_mono.
  - split.
    + constructor.
      intro n.
      simpl.
      apply le_ord_limit_r with n.
      now apply H.
    + simpl.
      constructor.
      intro n.
      apply le_ord_limit_r with n.
      now apply H.
Qed.

Lemma ord_pow_zero : forall a b, a ^° b = Zero -> a = Zero.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - easy.
  - simpl in H.
    apply ord_integral in H.
    destruct H.
    now apply IHb in H.
    assumption.
  - easy.
Qed.

Lemma lt_ord_mul_l : forall a b c, Zero <° c -> a <° b -> c *° a <° c *° b.
Proof.
  intros.
  apply (le_ord_mul_l (Succ a) b c) in H0.
  simpl in H0.
  apply (le_ord_add_succ (c *° a) c) in H.
  now apply (le_ord_trans (Succ (c *° a)) (c *° a +° c) (c *° b)).
Qed.

Lemma le_ord_mul_lr : forall a b c d, a <=° b -> c <=° d -> a *° c <=° b *° d.
Proof.
  intros.
  apply le_ord_trans with (b *° c).
  now apply le_ord_mul_r.
  now apply le_ord_mul_l.
Qed.

Lemma lt_ord_mul_lr : forall a b c d, Zero <° b -> a <=° b -> c <° d -> a *° c <° b *° d.
Proof.
  intros.
  assert (b *° c <° b *° d).
  now apply lt_ord_mul_l.
  apply le_ord_trans with (Succ (b *° c)).
  now apply le_ord_succ_mono, le_ord_mul_r.
  assumption.
Qed.

Lemma lt_ord_zero_pow : forall a b, Zero <° a -> Zero <° a ^° b.
Proof.
  intros.
  induction b.
  - apply le_ord_refl.
  - simpl.
    apply (le_ord_mul (a ^° b) _) in H.
    now apply le_ord_trans with (a ^° b).
  - simpl.
    apply le_ord_limit_r with 0.
    now apply H0.
Qed.

Lemma le_ord_pow_r : forall a b c, Zero <° a -> b <=° c -> a ^° b <=° a ^° c.
Proof.
  intros a b c H H1.
  revert a H.
  induction H1 as [c|b c|o c] ; intros.
  - now apply lt_ord_zero_pow.
  - simpl.
    apply (IHle_ord a) in H0 as ?.
    apply (le_ord_mul_r _ _ a) in H2.
    apply le_ord_trans with (a ^° c0 *° a) ; try assumption.
    induction c.
    + easy.
    + simpl.
     apply le_ord_mul_r.
     destruct H.
     * subst.
       apply le_ord_refl.
     * apply IHc in H as ?.
       apply le_ord_trans with (a ^° c0 *° a).
       now apply le_ord_mul.
       now apply IHc.
    + destruct H as [n H].
      simpl.
      now apply le_ord_limit_r with n, H3.
  - intros.
    simpl.
    constructor.
    intro.
    now apply H0.
Qed.

Lemma le_ord_pow : forall a b, Zero <° a -> Zero <° b -> a <=° a ^° b.
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - now apply nle_ord_zero_succ in H0.
  - simpl.
    now apply le_ord_mul2, lt_ord_zero_pow.
  - simpl.
    inversion_clear H1.
    destruct H3 as [n H3].
    apply le_ord_limit_r with n.
    apply H ; try assumption.
    now apply le_ord_Succ with c.
Qed.

(*
Lemma lt_ord_pow_r : forall a b c, Succ Zero <° a -> Succ Zero <° b -> b <° c -> a ^° b <° a ^° c.
Proof.
  intros.
  assert (Succ (a ^° b) <=° a ^° b +° b).
  apply (le_ord_add_lr (a ^° b) _ (Succ Zero) _). 
  apply le_ord_refl.
  now apply le_ord_succ_l.
  assert (a ^° b +° b <=° a ^° b *° b).
  induction b.
  now apply nle_ord_zero_succ in H0.
  simpl.
  assert (Succ (a ^° b *° a +° b) = a ^° b *° a +° b +° Succ Zero) by reflexivity.
  rewrite H3.
Abort. *)


(** Propriété vérifiée par les fonctions strictement croissantes de [nat -> Ord] *)
Definition infinite_limit o := forall n m, n < m -> o n <° o m.

(** Propriété vérifiée par les ordinaux bien formés i.e. dont tous les constructeurs [Limit] ont des fonctions strictement croissantes comme argument. *)
Inductive ilOrd : Ord -> Prop :=
  | ilOrd_Zero : ilOrd Zero
  | ilOrd_Succ : forall a, ilOrd a -> ilOrd (Succ a)
  | ilOrd_Limit : forall o, infinite_limit o -> (forall n, ilOrd (o n)) -> ilOrd (Limit o).


Lemma lt_ord_il_zero_neq_zero : forall a, ilOrd a -> Zero <° a <-> a <> Zero.
Proof.
  destruct a ; intros ; split ; intros.
  - now pose proof (nle_ord_zero_succ Zero).
  - contradiction.
  - easy.
  - apply le_ord_succ_mono.
    constructor.
  - easy.
  - apply (le_ord_limit_r (Succ Zero) o 1).
    inversion_clear H.
    pose proof (H1 0 1) as H1.
    apply (le_ord_trans (Succ Zero) (Succ (o 0)) (o 1)).
    apply le_ord_succ_mono ; try constructor.
    apply H1.
    constructor.
Qed.

Lemma le_ord_il_limit_r : forall a o, infinite_limit o -> (exists n, a <=° o n) <-> a <° Limit o.
Proof.
  intros ; split ; intro.
  - destruct H0 as [n H0].
    apply le_ord_limit_r with (S n).
    apply le_ord_succ_mono in H0.
    apply le_ord_trans with (Succ (o n)) ; try assumption.
    apply H.
    now unfold lt.
  - induction a.
    + exists 0.
      constructor.
    + apply le_ord_succ_l, IHa in H0.
      destruct H0 as [n H0].
      exists (S n).
      apply le_ord_succ_mono in H0.
      apply le_ord_trans with (Succ (o n)) ; try assumption.
      apply H.
      now unfold lt.
    + inversion_clear H0.
      destruct H3 as [n H3].
      exists n.
      apply le_ord_succ_l.
      now apply le_ord_Succ with c.
Qed.

Lemma le_ord_il_succ_r : forall a o, infinite_limit o -> Limit o <=° Succ a -> Limit o <=° a.
Proof.
  intros.
  inversion_clear H0.
  apply (le_ord_Limit o a).
  intro.
  pose proof (H1 (S n)) as H1.
  assert (Succ (o n) <=° o (S n)).
  apply (H n (S n)).
  now unfold lt.
  apply (le_ord_trans _ _ (Succ a)) in H0 ; try assumption.
  now apply le_ord_pred_mono in H0.
Qed.

Lemma lt_ord_il_succ_l : forall a o, infinite_limit o -> a <° Limit o -> Succ a <° Limit o.
Proof.
  intros.
  apply le_ord_il_limit_r in H0 ; try assumption.
  destruct H0 as [n H0].
  apply le_ord_il_limit_r ; try assumption.
  exists (S n).
  apply le_ord_trans with (Succ (o n)).
  now apply le_ord_succ_mono.
  apply H.
  now unfold lt.
Qed.

Lemma le_ord_il_zero : forall a, ilOrd a -> (Zero <° a <-> a <> Zero).
Proof.
  intros a H.
  induction a ; split ; intro.
  - now elim (lt_ord_irefl Zero).
  - contradiction.
  - easy.
  - apply le_ord_succ_mono.
    constructor.
  - easy.
  - apply le_ord_limit_r with 1.
    inversion_clear H.
    pose proof (H2 0 1) as H2.
    assert (Zero <° Succ (o 0)) by (apply le_ord_succ_mono ; constructor).
    apply le_ord_trans with (Succ (o 0)) ; try assumption.
    apply H2.
    constructor.
Qed.

Lemma ord_il_add : forall a b, ilOrd a -> ilOrd b -> ilOrd (a +° b).
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - assumption.
  - simpl.
    apply ilOrd_Succ, IHb ; try assumption.
    now inversion H0.
  - simpl.
    inversion_clear H1.
    apply ilOrd_Limit.
    + unfold infinite_limit.
      intros.
      apply H2 in H1.
      unfold lt_ord.
      assert (Succ (a +° o n) = a +° Succ (o n)) by easy.
      rewrite H4.
      now apply le_ord_add_l.
    + intro n.
      now apply H.
Qed.

Lemma ord_il_mul : forall a b, ilOrd a -> ilOrd b -> Zero <° a -> ilOrd (a *° b).
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - constructor.
  - simpl.
    apply ord_il_add ; try assumption.
    inversion_clear H0.
    now apply IHb.
  - simpl.
    apply ilOrd_Limit.
    + inversion_clear H1.
      unfold infinite_limit.
      intros.
      apply H3 in H1.
      now apply lt_ord_mul_l.
    + intro.
      inversion_clear H1.
      now apply H.
Qed.

(*
Lemma ord_il_pow :  forall a b, ilOrd a -> ilOrd b -> Succ Zero <° a -> ilOrd (a ^° b).
Proof.
  intros a b.
  revert a.
  induction b ; intros.
  - destruct a ; repeat constructor.
  - simpl.
    inversion_clear H0.
    apply ord_il_mul ; try assumption.
    + now apply IHb.
    + apply lt_ord_zero_pow.
      apply le_ord_trans with (Succ (Succ Zero)) ; try assumption.
      apply le_ord_succ_r, le_ord_refl.
  - simpl.
    constructor.
    intros n m ?.
    admit.
    intros.
    inversion_clear H1.
    now apply H.
Admitted.
*)

Fixpoint Ord_of_nat n :=
  match n with
  | 0 => Zero
  | S n => Succ (Ord_of_nat n)
  end.

Lemma ord_of_nat_add_1 : forall n, Ord_of_nat (S n) = Succ Zero +° (Ord_of_nat n).
Proof.
  induction n.
  - easy.
  - simpl.
    now rewrite <- IHn.
Qed.

Definition ω := Limit (Ord_of_nat).

Fact ord_il_omega : ilOrd ω.
Proof.
  apply ilOrd_Limit.
  unfold infinite_limit.
  induction 1.
  - simpl.
    apply le_ord_refl.
  - simpl.
    now apply le_ord_succ_r.
  - induction n.
    + constructor.
    + now apply ilOrd_Succ.
Qed.

(** 1 +#<sub>𝕆</sub># ω =#<sub>𝕆</sub># ω *)
Fact one_plus_omega_eq_omega : Succ Zero +° ω =° ω.
Proof.
  split ; simpl.
  - apply le_ord_Limit.
    intro.
    apply le_ord_limit_r with (S n).
    rewrite ord_of_nat_add_1.
    apply le_ord_refl.
  - apply le_ord_Limit.
    intro.
    apply le_ord_limit_r with n.
    induction n.
    + apply le_ord_Zero.
    + now apply le_ord_succ_mono.
Qed.

Lemma le_ord_omega_il : forall o, infinite_limit o -> ω <=° Limit o.
Proof.
  intros.
  assert (forall n, Ord_of_nat n <=° o n).
    induction n.
    constructor.
    simpl.
    pose proof (H n (S n)) as H.
    apply le_ord_succ_mono in IHn.
    apply le_ord_trans with (Succ (o n)) ; try assumption. 
    apply H.
    now unfold lt.
  assert (forall n, Ord_of_nat n <=° Limit o).
    intro n.
    pose proof (H0 n) as H0.
    now apply le_ord_limit_r in H0.
  now apply le_ord_Limit in H1.
Qed.

Lemma lt_ord_ord_il_omega_nat : forall a, ilOrd a -> a <° ω -> exists n, a = Ord_of_nat n.
Proof.
  induction a ; intros.
  - now exists 0.
  - inversion_clear H.
    apply IHa in H1.
    destruct H1 as [n H1].
    exists (S n).
    now rewrite H1.
  now apply le_ord_succ_l.
  - inversion_clear H0.
    apply le_ord_omega_il in H2.
    apply (le_ord_trans _ _ (Limit o)) in H1 ; try assumption.
    now apply nle_ord_succ in H1.
Qed.

Fixpoint ltb_ord_il_omega a :=
  match a with
  | Zero => true
  | Succ a => ltb_ord_il_omega a
  | Limit o => false
  end.

Lemma ltb_ord_il_omega_lt_omega : forall a, ilOrd a -> ltb_ord_il_omega a = true <->  a <° ω.
Proof.
  induction a ; intros ; split ; intros.
  - apply le_ord_limit_r with 1.
    now apply le_ord_refl.
  - reflexivity.
  - inversion_clear H.
    apply IHa in H1.
    apply H1 in H0.
    apply lt_ord_il_succ_l ; try assumption.
    pose proof ord_il_omega.
    now inversion H.
  - simpl.
    inversion_clear H.
    apply IHa in H1.
    apply H1.
    now apply le_ord_succ_l.
  - easy.
  - inversion_clear H0.
    apply (le_ord_omega_il o) in H2.
    apply (le_ord_trans _ _ (Limit o)) in H1 ; try assumption.
    now apply nle_ord_succ in H1.
Qed.

Fixpoint pow_iter a n :=
  match n with
  | 0 => Succ Zero
  | S n => a ^° (pow_iter a n)
  end.

(** ε#<sub>0</sub># est définit par [ε 0] *)
Fixpoint ε n :=
  match n with
  | 0 => Limit (pow_iter ω)
  | S n => Limit (pow_iter (ε n))
  end.

Fact omega_pow_ε0_eq_ord_ε0 : ω ^° ε 0 =° ε 0.
Proof.
  split.
  - simpl.
    apply le_ord_Limit.
    destruct n ; simpl.
    + apply le_ord_Limit.
      intro n.
      apply le_ord_limit_r with 1.
      simpl.
      apply le_ord_limit_r with n, le_ord_refl.
    + apply le_ord_limit_r with (S (S n)).
      simpl.
      apply le_ord_refl.
  - apply le_ord_Limit.
  destruct n ; simpl.
    + apply le_ord_limit_r with 0.
      simpl.
      apply le_ord_limit_r with 1, le_ord_refl.
    + apply le_ord_limit_r with n.
      apply le_ord_refl.
Qed.

(*
Fact ord_il_εn : forall n, ilOrd (ε n).
Proof.
  induction n.
  apply ilOrd_Limit.
  intros n m H.
  induction n ; intros.
  simpl.
  induction m.
  easy.
  simpl.
  admit.
  destruct m.
  simpl.
Abort.
*)

Inductive subε0Ord : Ord -> Prop :=
  | subε0Ord_Zero : subε0Ord Zero
  | subε0Ord_ω : subε0Ord ω
  | subε0Ord_Succ : forall a, subε0Ord a -> subε0Ord (Succ a)
  | subε0Ord_add : forall a b, subε0Ord a -> subε0Ord b -> b <> Zero -> subε0Ord (a +° b)
  | subε0Ord_mul : forall a b, subε0Ord a -> subε0Ord b -> a <> Zero -> a <> Succ Zero -> subε0Ord (a *° b)
  | subε0Ord_pow : forall a b, subε0Ord a -> subε0Ord b -> a <> Zero -> a <> Succ Zero -> subε0Ord (a ^° b).

Fixpoint sum_pow l b :=
  match l with
  | cons a t => (sum_pow t b) +° (b ^° a)
  | nil => Zero
  end.

(*
Lemma il_subε0Ord : forall a, subε0Ord a -> ilOrd a.
Proof.
  induction 1 ; intros.
  constructor.
  apply ord_il_omega.
  now constructor.
  now apply ord_il_add.
  apply ord_il_mul.
  assumption.
  assumption.
  apply (lt_ord_il_zero_neq_zero a) in IHsubε0Ord1.
  destruct IHsubε0Ord1.
  now apply H4.
  destruct a ; destruct b ; try easy.
  repeat constructor.
  simpl.
Admitted.

Lemma le_subε0Ord_ε0 : forall a, subε0Ord a -> a <=° ε 0.
Proof.
  induction 1 ; intros.
  constructor.
  apply le_ord_Limit.
  intro n.
  apply le_ord_limit_r with 1.
  admit.
  admit.
  inversion_clear IHsubε0Ord2.
  assumption.
Admitted.
*)

(* begin hide *)
End Ord.
(* end hide *)


(* begin hide *)
Module CantorOrd.
(* end hide *)

Inductive CantorOrd : Set :=
  | zero : CantorOrd
  | consC : CantorOrd -> CantorOrd -> CantorOrd.
Notation "ω^ a +: b" := (consC a b) (at level 49).

Fixpoint ord_of_cantor a :=
  match a with
  | zero => Ord.Zero
  | ω^a +: b => Ord.add_ord (Ord.pow_ord Ord.ω (ord_of_cantor a)) (ord_of_cantor b)
  end.

Fact cantor_subε0Ord : forall a, Ord.subε0Ord (ord_of_cantor a).
Proof.
  induction a.
  - constructor.
  - simpl.
    destruct a2.
    + now repeat constructor.
    + constructor ; try assumption.
      * now repeat constructor.
      * intro.
        simpl in H.
        apply Ord.ord_add_zero in H.
        destruct H as [H _].
        now apply Ord.ord_pow_zero in H.
Qed.

Fixpoint eqb_cantor a b :=
  match a, b with
  | zero, zero => true
  | ω^a' +: a'', ω^b' +: b'' => andb (eqb_cantor a' b') (eqb_cantor a'' b'')
  | _, _ => false
  end.
Infix "=?" := eqb_cantor (at level 70).

Fixpoint ltb_cantor a b :=
  match a, b with
  | _, zero => false
  | zero, _ => true
  | ω^a +: a', ω^b +: b' => if ltb_cantor a b
    then true
    else (if eqb_cantor a b
      then ltb_cantor a' b'
      else false)
  end.
Infix "<?" := ltb_cantor (at level 70).

Definition lt_cantor a b := ltb_cantor a b = true.
Infix "<" := lt_cantor.

Definition leb_cantor a b := orb (ltb_cantor a b) (eqb_cantor a b).
Infix "<=?" := leb_cantor (at level 70).

Definition le_cantor a b := leb_cantor a b = true.
Infix "<=" := le_cantor.

Fixpoint add_cantor a b :=
  match a, b with
  | zero, _ => b
  | _, zero => a
  | ω^a' +: a'', ω^b' +: b'' =>
    if a' <? b'
     then b
     else (if b' <? a'
       then ω^a' +: (add_cantor a'' b)
       else ω^a' +: ω^b' +: (add_cantor a'' b''))
  end.
Infix "+" := add_cantor.

Fixpoint sub_cantor a b :=
  match a, b with
  | a, zero => a
  | zero, _ => zero
  | ω^a' +: a'', ω^b' +: b'' =>
    if a <? b
      then zero
      else (if b' <? a'
        then a
        else sub_cantor a'' b'')
  end.
Infix "-" := sub_cantor.

Fixpoint mul_cantor a b :=
  match a, b with
  | zero, _ => zero
  | _, zero => zero
  | ω^a' +: a'', ω^b' +: b'' => ω^(a' + b') +: (mul_cantor a b'')
  end.
Infix "*" := mul_cantor.

(** [merge_cantor] est la version commutative de [+] *)
Fixpoint merge_cantor a := fix merge_cantor' b :=
  match a, b with
  | zero, _ => b
  | _, zero => a
  | ω^a' +: a'', ω^b' +: b'' =>
    if a' <? b'
     then ω^b' +: (merge_cantor' b'')
     else (if b' <? a'
       then ω^a' +: (merge_cantor a'' b)
       else ω^a' +: ω^b' +: (merge_cantor a'' b''))
  end.

Fixpoint remove_cantor a b :=
  match a, b with
  | a, zero => a
  | zero, _ => zero
  | ω^a' +: a'', ω^b' +: b'' =>
    if a <? b
      then zero
      else (if b' <? a'
        then ω^a' +: remove_cantor a'' b
        else remove_cantor a'' b'')
  end.
Infix "--" := remove_cantor (at level 50).

Definition max_cantor a b := if a <? b then b else a.

Lemma eq_cantor_inj : forall a a' b b', ω^a +: a' = ω^b +: b' <-> (a = b /\ a' = b').
Proof.
  intros ; split ; intro.
  - injection H ; intros ; now split.
  - now destruct H ; subst.
Qed.

Lemma eqb_cantor_refl : forall a, a =? a = true.
Proof.
  induction a.
  - easy.
  - simpl.
    now rewrite IHa1, IHa2.
Qed.

Lemma eqb_cantor_eq : forall a b, a =? b = true <-> a = b.
Proof.
  induction a ; intros ; split ; intros.
  - destruct b.
    + reflexivity.
    + now simpl in H.
  - now rewrite <- H.
  - destruct b.
    + now simpl in H.
    + apply eq_cantor_inj.
      simpl in H.
      apply andb_prop in H.
      destruct H.
      split.
      * now apply IHa1.
      * now apply IHa2.
  - rewrite <- H.
    simpl.
    apply andb_true_intro.
    split.
    + now apply IHa1.
    + now apply IHa2.
Qed.

Lemma neqb_cantor_neq : forall a b, a =? b = false <-> a <> b.
Proof.
  induction a ; intro ; split ; intro ; destruct b ; try easy.
  - intro.
    injection H0 ; intros.
    now rewrite H1, H2, eqb_cantor_refl in H.
  - destruct (eqb_cantor (ω^a1 +: a2) (ω^b1 +: b2)) as []eqn:?.
    + now apply eqb_cantor_eq in Heqb.
    + reflexivity.
Qed.

Lemma eqb_cantor_sym : forall a b, (a =? b) = (b =? a).
Proof.
  intros.
  destruct (a =? b) as []eqn:?.
  - now apply eqb_cantor_eq, eq_sym, eqb_cantor_eq in Heqb0.
  - destruct (b =? a) as []eqn:?.
    + apply eqb_cantor_eq, eq_sym, eqb_cantor_eq in Heqb1.
      now rewrite Heqb0 in Heqb1.
    + reflexivity.
Qed.

Lemma leb_cantor_zero_r : forall a, a <=? zero = true -> a = zero.
Proof.
  now induction a.
Qed.

Lemma leb_cantor_zero_l : forall a, zero <=? a = true.
Proof.
  now destruct a.
Qed.

Lemma ltb_cantor_zero_r : forall a, a <? zero = false.
Proof.
  now destruct a.
Qed.

Lemma ltb_cantor_zero_l : forall a, a <> zero <-> zero <? a = true.
Proof.
  now induction a.
Qed.

Lemma ltb_cantor_irefl : forall a, a <? a = false.
Proof.
  induction a.
  - easy.
  - simpl.
    now rewrite IHa1, IHa2, eqb_cantor_refl.
Qed.

Lemma ltb_cantor_leb_incl : forall a b, a <? b = true -> a <=? b = true.
Proof.
  intros.
  unfold leb_cantor.
  now rewrite H.
Qed.

Lemma leb_cantor_refl : forall a, a <=? a = true.
Proof.
  intros.
  unfold leb_cantor.
  rewrite (eqb_cantor_refl a).
  now destruct (a <? a).
Qed.

Lemma ltb_cantor_trichotomy : forall a b, a <? b = true \/ a =? b = true \/ b <? a = true.
Proof.
  induction a.
  - destruct b.
    + now right ; left.
    + now left.
  - induction b.
    + now right ; right.
    + destruct (IHa1 b1) as [|[|]].
      * left.
        simpl.
        now rewrite H.
      * destruct (IHa2 b2) as [|[|]].
        -- left.
           simpl.
           apply eqb_cantor_eq in H.
           now rewrite H, ltb_cantor_irefl, eqb_cantor_refl.
        -- right ; left.
           simpl.
           now rewrite H, H0.
        -- right ; right.
           simpl.
           apply eqb_cantor_eq in H.
           now rewrite H, ltb_cantor_irefl, eqb_cantor_refl.
      * right ; right.
        simpl.
        now rewrite H.
Qed.

Lemma leb_cantor_dichotomy : forall a b, a <=? b = true \/ b <=? a = true.
Proof.
  intros.
  destruct (ltb_cantor_trichotomy a b) as [|[|]]; unfold leb_cantor.
  - left.
    now rewrite H.
  - left.
    rewrite H.
    now destruct (a <? b).
  - right.
    now rewrite H.
Qed.

Lemma ltb_cantor_st : forall a b, a <? b = true -> a =? b = false.
Proof.
  induction a ; intros.
  - apply ltb_cantor_zero_l in H.
    now destruct b.
  - destruct (ω^a1 +: a2 =? b) as []eqn:?.
    + apply eqb_cantor_eq in Heqb0.
      now rewrite Heqb0, ltb_cantor_irefl in H.
    + reflexivity.
Qed.

Lemma ltb_cantor_st2 : forall a b, a =? b = true ->  a <? b = false.
Proof.
  intros.
  apply eqb_cantor_eq in H.
  subst.
  now apply ltb_cantor_irefl.
Qed.

Lemma ltb_cantor_antisym2 : forall a b, a <? b = false <-> (a =? b = true \/ b <? a = true).
Proof.
  induction a ; intro ; split ; intro.
  - left.
    now destruct b.
  - destruct H.
    + apply eqb_cantor_eq in H.
      now rewrite <- H.
    + now destruct b.
  - destruct (ltb_cantor_trichotomy (ω^a1 +: a2) b) as [|[|]].
    + now rewrite H0 in H.
    + now left.
    + now right.
  - destruct H.
    + apply eqb_cantor_eq in H.
      rewrite H.
      now apply ltb_cantor_irefl.
    + destruct ((ω^a1 +: a2) <? b) as []eqn:?.
      * destruct b.
        -- now rewrite ltb_cantor_zero_r in Heqb0.
        -- simpl in H, Heqb0.
           destruct (b1 <? a1) as []eqn:?.
           ++ pose proof (IHa1 b1) as IHa1.
              assert (a1 =? b1 = true \/ b1 <? a1 = true) by (now right).
              apply IHa1 in H0.
              rewrite H0 in Heqb0.
              destruct (a1 =? b1) as []eqn:?.
              ** pose proof (eqb_cantor_sym b1 a1).
                 rewrite Heqb1 in H1.
                 apply ltb_cantor_st2 in H1.
                 now rewrite H1 in Heqb.
              ** easy.
           ++ destruct (ltb_cantor_trichotomy b1 a1) as [|[|]].
              ** now rewrite H0 in Heqb.
              ** pose proof (eqb_cantor_sym b1 a1).
                 rewrite H0 in H.
                 rewrite H1 in H0.
                 rewrite H0 in Heqb0.
                 apply ltb_cantor_st2 in H0.
                 rewrite H0 in Heqb0.
                 pose proof (IHa2 b2) as IHa2.
                 assert (a2 =? b2 = true \/ b2 <? a2 = true) by (now right).
                 apply IHa2 in H2.
                 now rewrite H2 in Heqb0.
              ** apply ltb_cantor_st in H0.
                 pose proof (eqb_cantor_sym a1 b1).
                 rewrite H1 in H0.
                 now rewrite H0 in H.
      * reflexivity.
Qed.

Lemma ltb_cantor_antisym : forall a b, a <? b = true -> (a =? b = false /\ b <? a = false).
Proof.
  induction a ; intros ; split.
  - apply ltb_cantor_zero_l, neqb_cantor_neq in H.
    now rewrite eqb_cantor_sym.
  - now apply ltb_cantor_zero_r.
  - destruct b.
    + now apply neqb_cantor_neq.
    + simpl in H.
      simpl.
      unfold andb.
      destruct (a1 <? b1) as []eqn:?.
      * apply (IHa1 b1) in Heqb.
        destruct Heqb as [Heqb _].
        now rewrite Heqb.
      * apply ltb_cantor_antisym2 in Heqb.
        destruct Heqb.
        -- rewrite H0 in H.
           rewrite H0.
           apply (IHa2 b2) in H.
           now destruct H as [H _].
        -- apply ltb_cantor_st in H0.
           rewrite eqb_cantor_sym in H0.
           now rewrite H0 in H.
  - destruct b.
    + now rewrite ltb_cantor_zero_r in H.
    + simpl in H.
      simpl.
      destruct (ltb_cantor_trichotomy a1 b1) as [|[|]].
      * apply (IHa1 b1) in H0.
        destruct H0.
        now rewrite H1, eqb_cantor_sym, H0.
      * rewrite eqb_cantor_sym in H0.
        rewrite H0.
        apply ltb_cantor_st2 in H0 as H1.
        rewrite H1.
        apply ltb_cantor_antisym2 in H1.
        destruct H1.
        -- rewrite eqb_cantor_sym in H1.
           rewrite H1 in H.
           apply ltb_cantor_st2 in H1.
           rewrite H1 in H.
           apply (IHa2 b2) in H.
           now destruct H as [_ H].
        -- rewrite eqb_cantor_sym in H0.
           apply ltb_cantor_st2 in H0.
           now rewrite H0 in H1.
      * destruct (a1 <? b1) as []eqn:?.
        apply (IHa1 b1) in Heqb.
        destruct Heqb as [_ Heqb].
        now rewrite Heqb in H0.
        -- destruct (a1 =? b1) as []eqn:?.
           ++ apply ltb_cantor_st in H0.
              now rewrite eqb_cantor_sym, Heqb0 in H0.
           ++ easy.
Qed.

Lemma ltb_cantor_trans : forall a b c, a <? b = true -> b <? c = true -> a <? c = true.
Proof.
  intros a b c.
  revert a b.
  induction c ; intros.
  - pose proof (ltb_cantor_zero_r b).
    now rewrite H1 in H0.
  - destruct a.
    + easy.
    + simpl.
      simpl in H.
      destruct b.
      * easy.
      * simpl in H0.
        destruct (a1 <? c1) as []eqn:?.
        -- easy.
        -- apply ltb_cantor_antisym2 in Heqb.
           destruct Heqb.
           ++ rewrite H1.
              apply eqb_cantor_eq in H1.
              rewrite H1 in H.
              destruct (ltb_cantor_trichotomy b1 c1) as [|[|]].
              ** apply ltb_cantor_antisym in H2.
                 destruct H2 as [H2 H2'].
                 rewrite H2' in H.
                 rewrite eqb_cantor_sym in H2.
                 now rewrite H2 in H.
              ** rewrite H2 in H0.
                 apply ltb_cantor_st2 in H2 as H3.
                 rewrite eqb_cantor_sym in H2.
                 rewrite H2 in H.
                 apply ltb_cantor_st2 in H2.
                 rewrite H2 in H.
                 rewrite H3 in H0.
                 now apply IHc2 in H.
              ** apply ltb_cantor_antisym in H2.
                 destruct H2.
                 rewrite H3 in H0.
                 rewrite eqb_cantor_sym in H2.
                 now rewrite H2 in H0.
           ++ destruct (a1 <? b1) as []eqn:? ; destruct (ltb_cantor b1 c1) as []eqn:?.
              ** apply IHc1 in Heqb.
                 apply ltb_cantor_antisym in Heqb.
                 destruct Heqb as [_ Heqb].
                 now rewrite Heqb in H1.
                 easy.
              ** destruct (b1 =? c1) as []eqn:?.
                 --- apply eqb_cantor_eq in Heqb1.
                     rewrite <- Heqb1 in H1.
                     apply ltb_cantor_antisym in Heqb.
                     destruct Heqb as [_ Heqb].
                     now rewrite Heqb in H1.
                 --- easy.
              ** destruct (a1 =? b1) as []eqn:?.
                 --- apply eqb_cantor_eq in Heqb1.
                     rewrite <- Heqb1 in Heqb0.
                     apply ltb_cantor_antisym in Heqb0.
                     destruct Heqb0 as [_ Heqb0].
                     now rewrite Heqb0 in H1.
                 --- easy.
              ** destruct (a1 =? b1) as []eqn:? ; destruct (b1 =? c1) as []eqn:?.
                 --- apply eqb_cantor_eq in Heqb1.
                     apply eqb_cantor_eq in Heqb2.
                     now rewrite Heqb1, Heqb2, ltb_cantor_irefl in H1.
                 --- easy.
                 --- easy.
                 --- easy.
Qed.

Lemma ltb_leb_cantor_trans : forall a b c, a <? b = true -> b <=? c = true -> a <? c = true.
Proof.
  intros.
  unfold leb_cantor in H0.
  unfold orb in H0.
  destruct (b <? c) as []eqn:?.
  - now apply (ltb_cantor_trans a b c) in H.
  - apply eqb_cantor_eq in H0.
    now rewrite <- H0.
Qed.

Lemma leb_ltb_cantor_trans : forall a b c, a <=? b = true -> b <? c = true -> a <? c = true.
Proof.
  intros.
  unfold leb_cantor in H.
  unfold orb in H.
  destruct (a <? b) as []eqn:?.
  - now apply (ltb_cantor_trans a b c) in Heqb0.
  - apply eqb_cantor_eq in H.
    now rewrite H.
Qed.

Lemma leb_cantor_trans : forall a b c, a <=? b = true -> b <=? c = true -> a <=? c = true.
Proof.
  intros.
  unfold leb_cantor, orb in *.
  destruct (a <? b) as []eqn:? ; destruct (b <? c) as []eqn:?.
  * apply (ltb_cantor_trans a b c) in Heqb0 ; try assumption.
    now apply ltb_cantor_leb_incl.
  * apply eqb_cantor_eq in H0.
    subst.
    now apply ltb_cantor_leb_incl.
  * apply eqb_cantor_eq in H.
    subst.
    now apply ltb_cantor_leb_incl.
  * apply eqb_cantor_eq in H.
    apply eqb_cantor_eq in H0.
    subst.
    apply leb_cantor_refl.
Qed.

Lemma ltb_cantor_negb_leb : forall a b, a <? b = negb (b <=? a).
Proof.
  intros.
  unfold leb_cantor.
  destruct (a <? b) as []eqn:?.
  - destruct (ltb_cantor_antisym a b) ; try assumption.
    rewrite eqb_cantor_sym in H.
    now rewrite H, H0.
  - apply (ltb_cantor_antisym2 a b) in Heqb0.
    destruct Heqb0 ; try rewrite eqb_cantor_sym in H ; rewrite H.
    + now destruct (b <? a).
    + easy.
Qed.

Lemma ltb_cantor_cons_mono : forall a b c, a <? b = true <-> ω^c +: a <? ω^c +: b = true.
Proof.
  intros ; split ; intro.
  - simpl.
    now rewrite ltb_cantor_irefl, eqb_cantor_refl.
  - simpl in H.
    now rewrite ltb_cantor_irefl, eqb_cantor_refl in H.
Qed.

Lemma leb_cantor_cons_mono : forall a b c, a <=? b = true <-> ω^c +: a <=? ω^c +: b = true.
Proof.
  intros ; split ; intro.
  - unfold leb_cantor.
    simpl.
    now rewrite ltb_cantor_irefl, eqb_cantor_refl.
  - unfold leb_cantor in H.
    simpl in H.
    now rewrite ltb_cantor_irefl, eqb_cantor_refl in H.
Qed.

Lemma ltb_cantor_exp : forall b, b <? (ω^b +: zero) = true.
Proof.
  induction b.
  - easy.
  - simpl.
    assert (b1 <? (ω^b1 +: b2) = true).
      destruct b1.
      easy.
      simpl.
      simpl in IHb1.
      destruct (b1_1 <? (ω^b1_1 +: b1_2)) as []eqn:?.
      easy.
      destruct (b1_1 =? (ω^b1_1 +: b1_2)) as []eqn:?.
      now rewrite ltb_cantor_zero_r in IHb1.
      assumption.
    now rewrite H.
Qed.

Lemma ltb_cantor_exp_cons : forall a b, a <? (ω^a +: b) = true.
Proof.
  intros.
  pose proof (ltb_cantor_exp a).
  apply ltb_leb_cantor_trans with (ω^a +: zero) ; try easy.
  unfold leb_cantor.
  simpl.
  rewrite ltb_cantor_irefl, eqb_cantor_refl.
  now destruct b.
Qed.

Lemma sub_cantor_zero_r : forall a, a - zero = a.
Proof.
  intro a.
  now destruct a.
Qed.

Lemma sub_cantor_zero : forall a, a - a = zero.
Proof.
  intro a.
  induction a as [| a' _ a IHa ].
  - reflexivity.
  - simpl.
    repeat rewrite ltb_cantor_irefl.
    now rewrite eqb_cantor_refl.
Qed.

Lemma remove_cantor_zero_r : forall a, a -- zero = a.
Proof.
  now destruct a.
Qed.

Lemma remove_cantor_zero_l : forall a, zero -- a = zero.
Proof.
  now destruct a.
Qed.

Lemma remove_cantor_zero : forall a, a -- a = zero.
Proof.
  intro a.
  induction a as [| a' _ a IHa ].
  - reflexivity.
  - simpl.
    repeat rewrite ltb_cantor_irefl.
    now rewrite eqb_cantor_refl.
Qed.

Lemma ltb_cantor_max_l : forall a b c, (max_cantor a b) <? c = true <-> a <? c = true /\ b <? c = true.
Proof.
  destruct a ; destruct b ; intros ; split ; intro.
  - split ; destruct H ; now destruct c.
  - now destruct H.
  - split ; now destruct c.
  - now destruct H.
  - split ; now destruct c.
  - now destruct H.
  - split.
    + destruct c ; unfold max_cantor in H.
      * now destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      * destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
        -- now apply ltb_cantor_trans with (ω^b1 +: b2).
        -- assumption.
    + unfold max_cantor in H.
      destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      * assumption.
      * pose proof (ltb_cantor_negb_leb (ω^a1 +: a2) (ω^b1 +: b2)).
        destruct (ω^b1 +: b2 <=? ω^a1 +: a2) as []eqn:?.
        -- now apply leb_ltb_cantor_trans with (ω^a1 +: a2).
        -- now rewrite Heqb in H0.
  - destruct H.
    unfold max_cantor.
    now destruct (ω^a1 +: a2 <? ω^b1 +: b2).
Qed.

Lemma leb_cantor_max_l : forall a b c, (max_cantor a b) <=? c = true <-> a <=? c = true /\ b <=? c = true.
Proof.
  destruct a ; destruct b ; intros ; split ; intro.
  - split ; destruct H ; now destruct c.
  - now destruct H.
  - split ; now destruct c.
  - now destruct H.
  - split ; now destruct c.
  - now destruct H.
  - split.
    + destruct c ; unfold max_cantor in H.
      now destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      * now apply ltb_cantor_leb_incl, ltb_leb_cantor_trans with (ω^b1 +: b2).
      * assumption.
    + unfold max_cantor in H.
      destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      * assumption.
      * pose proof (ltb_cantor_negb_leb (ω^a1 +: a2) (ω^b1 +: b2)).
        destruct (ω^b1 +: b2 <=? ω^a1 +: a2) as []eqn:?.
        -- now apply leb_cantor_trans with (ω^a1 +: a2).
        -- now rewrite Heqb in H0.
  - destruct H.
    unfold max_cantor.
    now destruct (ω^a1 +: a2 <? ω^b1 +: b2).
Qed.

Definition degree a :=
  match a with
  | zero => zero
  | ω^a' +: a'' => a'
  end.

Lemma ltb_cantor_degree : forall a, a <> zero -> degree a <? a = true.
Proof.
  induction a ; intros.
  - easy.
  - simpl.
    pose proof (ltb_cantor_exp a1).
    assert (leb_cantor (ω^a1 +: zero) (ω^a1 +: a2) = true).
      unfold leb_cantor.
      simpl.
      rewrite ltb_cantor_irefl, eqb_cantor_refl.
      now destruct a2.
    now apply ltb_leb_cantor_trans with (ω^a1 +: zero).
Qed.

Lemma leb_cantor_degree : forall a, degree a <=? a = true.
Proof.
  destruct a.
  - easy.
  - unfold leb_cantor.
    now rewrite ltb_cantor_degree.
Qed.

Lemma leb_cantor_degree_mono : forall a b, a <=? b = true -> degree a <=? degree b = true.
Proof.
  destruct a ; destruct b ; intro H.
  - now rewrite leb_cantor_refl.
  - now rewrite leb_cantor_zero_l.
  - now apply leb_cantor_zero_r in H.
  - simpl.
    unfold leb_cantor in *.
    destruct (ω^a1 +: a2 <? ω^b1 +: b2)eqn:Heqb.
    + simpl in Heqb.
      now destruct (a1 <? b1) ; [| destruct (a1 =? b1) ].
    + simpl in H.
      apply andb_prop in H.
      destruct H as [H _].
      rewrite H.
      now destruct (a1 <? b1).
Qed.

Lemma merge_cantor_max_degree : forall a b, degree (merge_cantor a b) = max_cantor (degree a) (degree b).
Proof.
  destruct a ; destruct b.
  - easy.
  - now destruct b1.
  - now destruct a1.
  - destruct (a1 <? b1) as []eqn:?.
    + simpl.
      unfold max_cantor.
      now rewrite Heqb.
    + now destruct (b1 <? a1) as []eqn:? ; simpl ; unfold max_cantor ; rewrite Heqb, Heqb0.
Qed.

Inductive normal : CantorOrd -> Prop :=
  | normal_zero : normal zero
  | normal_cons : forall a b, normal a -> normal b -> degree b <= a -> normal (ω^a +: b).

Fixpoint normal_bool a :=
  match a with
  | zero => true
  | ω^a' +: a'' => andb (degree a'' <=? a') (andb (normal_bool a') (normal_bool a''))
  end.

Inductive CantorNormalOrd :=
  Normal a : normal a -> CantorNormalOrd.

Definition lt_cantorN a b :=
  match a, b with
  | Normal a' _, Normal b' _ => a' < b'
  end.

Definition le_cantorN a b :=
  match a, b with
  | Normal a' _, Normal b' _ => a' <= b'
  end.

Definition zeroN := Normal zero normal_zero.

Lemma normal_degree : forall a, normal a -> normal (degree a).
Proof.
  intros.
  destruct a.
  - constructor.
  - now inversion H.
Qed.

Definition degreeN a :=
  match a with
  | Normal zero _ => zeroN
  | Normal a Na => Normal (degree a) (normal_degree a Na)
  end.

Definition normal_value a := match a with Normal a _ => a end.

Lemma normality a : normal (normal_value a).
Proof.
  now destruct a.
Qed.

Lemma normal_normal_bool : forall a, normal a <-> normal_bool a = true.
Proof.
  induction a ; split ; intro.
  - easy.
  - constructor.
  - simpl.
    inversion_clear H.
    unfold le_cantor in H2.
    apply IHa1 in H0.
    apply IHa2 in H1.
    now rewrite H0, H1, H2.
  - simpl in H.
    destruct (degree a2 <=? a1) as []eqn:? ; try easy.
    destruct (normal_bool a1) as []eqn:? ; try easy.
    destruct (normal_bool a2) as []eqn:? ; try easy.
    constructor.
    + now apply IHa1.
    + now apply IHa2.
    + easy.
Qed.

Lemma leb_normal_antisym : forall a b, normal a -> normal b -> a <=? b = true -> b <=? a = true -> a = b.
Proof.
  destruct a.
  - intros.
    now apply leb_cantor_zero_r in H2.
  - destruct b ; intros.
    + now apply leb_cantor_zero_r in H1.
    + unfold leb_cantor in *.
      destruct (ω^a1 +: a2 <? ω^b1 +: b2) as []eqn:?.
      * apply ltb_cantor_antisym in Heqb.
        destruct Heqb as [_ Heqb].
        destruct (ω^b1 +: b2 <? ω^a1 +: a2) as []eqn:?.
        -- now rewrite Heqb in Heqb0.
        -- simpl in H2.
           apply andb_prop in H2.
           apply eq_sym, eq_cantor_inj.
           now split ; apply eqb_cantor_eq.
      * simpl in H1.
        apply andb_prop in H1.
        apply eq_cantor_inj.
        now split ; apply eqb_cantor_eq.
Qed.

Lemma merge_cantor_zero_r : forall a, merge_cantor a zero = a.
Proof.
  now destruct a.
Qed.

Lemma merge_cantor_zero_l : forall a, merge_cantor zero a = a.
Proof.
  now destruct a.
Qed.

Lemma merge_cantor_zero_lr : forall a b, merge_cantor a b = zero -> a = zero /\ b = zero.
Proof.
  intros.
  destruct a ; destruct b ; try easy.
  simpl in H.
  destruct (a1 <? b1).
  easy.
  now destruct (b1 <? a1).
Qed.

Lemma merge_cantor_normal : forall a b, normal a -> normal b -> normal (merge_cantor a b).
Proof.
  induction a ; induction b ; intros ; try easy.
  simpl.
  destruct (a1 <? b1) as []eqn:?.
  - constructor.
    + now inversion H0.
    + apply IHb2 in H as ?.
      assumption.
      now inversion H0.
    + apply IHb2 in H as ?.
      destruct b2.
      unfold le_cantor, leb_cantor.
      simpl.
      now rewrite Heqb.
      destruct (a1 <? b2_1).
      * now inversion H0.
      * now destruct (b2_1 <? a1) ; unfold le_cantor, leb_cantor ; simpl ; rewrite Heqb.
      * now inversion H0.
  - destruct (b1 <? a1) as  []eqn:? ; constructor ; try now inversion H.
    + now apply IHa2 ; try now inversion H.
    + pose proof (merge_cantor_max_degree a2 (ω^b1 +: b2)).
      rewrite H1.
      apply leb_cantor_max_l ; split.
      now inversion H.
      pose proof (ltb_cantor_negb_leb a1 b1).
      rewrite Heqb in H2.
      now destruct (b1 <=? a1) as []eqn:?.
    + destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H1 in *.
      constructor ; try now inversion H0.
      * apply IHa2 ; try now inversion H ; try now inversion H0.
      * rewrite (merge_cantor_max_degree a2 b2).
        apply eqb_cantor_eq in H1.
        subst.
        apply leb_cantor_max_l ; split.
        -- now inversion H.
        -- now inversion H0.
    + destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H1 in *.
      apply eqb_cantor_eq in H1.
      subst.
      simpl.
      unfold le_cantor.
      now rewrite leb_cantor_refl.
Qed.

(* begin show *)
(** [merge_cantor] est commutative *)
Lemma merge_cantor_normal_comm : forall a b, normal a -> normal b -> (merge_cantor a b = merge_cantor b a).
Proof.
  induction a ; intros.
  - now destruct b.
  - induction b.
    + easy.
    + simpl.
      destruct (a1 <? b1) as []eqn:? ; [| destruct (b1 <? a1) as []eqn:? ].
      * apply ltb_cantor_antisym in Heqb as ?.
        destruct H1.
        rewrite H2.
        apply eq_cantor_inj ; split ; try easy.
        apply IHb2.
        now inversion H0.
      * apply eq_cantor_inj ; split ; try easy.
        apply IHa2 ; try easy.
        now inversion H.
      * destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H1 in *.
        apply eqb_cantor_eq in H1.
        subst.
        repeat (apply eq_cantor_inj ; split ; try easy).
        apply IHa2.
        now inversion H.
        now inversion H0.
Qed.
(* end show *)

Lemma merge_cantor_normal_cons : forall a b c, normal a -> normal b -> normal c -> degree b <=? a = true -> degree c <=? a = true -> merge_cantor (ω^a +: b) c = ω^a +: merge_cantor b c /\ merge_cantor b (ω^a +: c) = ω^a +: merge_cantor b c.
Proof.
  intros a b.
  revert a.
  induction b ; destruct c ; intros ; split.
  easy.
  easy.
  simpl.
  destruct (a <? c1) as []eqn:?.
  simpl in H3.
  pose proof (ltb_cantor_negb_leb a c1).
  rewrite H3 in H4.
  now rewrite H4 in Heqb.
  destruct (c1 <? a) as []eqn:?.
  reflexivity.
  now destruct c2.
  easy.
  easy.
  rewrite merge_cantor_zero_r.
  simpl.
  destruct (b1 <? a) as []eqn:?.
  reflexivity.
  simpl in H2.
  unfold leb_cantor in H2.
  rewrite Heqb in H2.
  simpl in H2.
  apply eqb_cantor_eq in H2.
  subst.
  rewrite ltb_cantor_irefl.
  now rewrite merge_cantor_zero_r.
  simpl.
  destruct (a <? c1) as []eqn:?.
  simpl in H3.
  pose proof (ltb_cantor_negb_leb a c1).
  rewrite H3 in H4.
  now rewrite H4 in Heqb.
  destruct (c1 <? a) as []eqn:?.
  reflexivity.
  destruct (ltb_cantor_trichotomy a c1) as [|[|]] ; try now rewrite H4 in *.
  apply eqb_cantor_eq in H4.
  subst.
  destruct (b1 <? c1) as []eqn:?.
  reflexivity.
  simpl in H2.
  unfold leb_cantor in H2.
  rewrite Heqb1 in H2.
  simpl in H2.
  apply eqb_cantor_eq in H2.
  subst.
  rewrite ltb_cantor_irefl.
  inversion_clear H1.
  apply (IHb2 c1 c2) in H2 ; try assumption.
  destruct H2 as [H2 _].
  rewrite <- H2.
  now destruct c2.
  now inversion H0.
  now inversion H0.
  simpl.
  destruct (b1 <? a) as []eqn:?.
  reflexivity.
  destruct (b1 <? c1) as []eqn:?.
  destruct (a <? b1) as []eqn:?.
  simpl in H2.
  pose proof (ltb_cantor_negb_leb a b1).
  rewrite H2 in H4.
  now rewrite H4 in Heqb1.
  destruct (ltb_cantor_trichotomy a b1) as [|[|]] ; try now rewrite H4 in *.
  apply eqb_cantor_eq in H4.
  subst.
  simpl in H3.
  pose proof (ltb_cantor_negb_leb b1 c1).
  rewrite H3 in H4.
  now rewrite H4 in Heqb0.
  simpl in H2.
  unfold leb_cantor in H2.
  rewrite Heqb in H2.
  simpl in H2.
  apply eqb_cantor_eq in H2.
  subst.
  rewrite ltb_cantor_irefl.
  destruct (c1 <? a) as []eqn:?.
  reflexivity.
  destruct (ltb_cantor_trichotomy a c1) as [|[|]] ; try now rewrite H2 in *.
  apply eqb_cantor_eq in H2.
  subst.
  inversion_clear H1.
  apply (IHb2 c1 c2) in H2 ; try assumption.
  destruct H2 as [_ H2].
  now rewrite H2.
  now inversion H0.
  now inversion H0.
Qed.

Lemma merge_cantor_normal_cons_l : forall a b c, normal (ω^a +: b) -> normal c -> degree c <=? a = true -> merge_cantor (ω^a +: b) c = ω^a +: merge_cantor b c.
Proof.
  intros.
  destruct (merge_cantor_normal_cons a b c) as [? _] ; try now inversion H.
Qed.

Lemma merge_cantor_normal_cons_r : forall a b c, normal (ω^b +: c) -> normal a -> degree a <=? b = true -> merge_cantor a (ω^b +: c) = ω^b +: merge_cantor a c.
Proof.
  intros.
  destruct (merge_cantor_normal_cons b a c) as [_ ?] ; try now inversion H.
Qed.

(** [merge_cantor] est associtative *)
Lemma merge_cantor_normal_assoc : forall a b c, normal a -> normal b -> normal c -> merge_cantor (merge_cantor a b) c = merge_cantor a (merge_cantor b c).
Proof.
  induction a ; induction b ; induction c ; intros ; try easy.
  - now repeat rewrite merge_cantor_zero_l.
  - now repeat rewrite merge_cantor_zero_r.
  - clear IHa1 IHb1 IHc1.
    simpl.
    destruct (a1 <? b1) as []eqn:?.
    + destruct (b1 <? c1) as []eqn:?.
      * apply (ltb_cantor_trans a1 b1 c1) in Heqb as ?.
        rewrite H2.
        simpl.
        rewrite Heqb0.
        apply eq_cantor_inj ; split ; try easy.
        apply IHc2 in H as ?.
        simpl in H3.
        rewrite Heqb in H3.
        now rewrite <- H3.
        assumption.
        now inversion H1.
        assumption.
      * destruct (c1 <? b1) as []eqn:?.
        rewrite Heqb.
        simpl.
        rewrite Heqb0, Heqb1.
        apply eq_cantor_inj ; split ; try easy.
        apply IHb2 ; try assumption.
        now inversion H0.
        rewrite Heqb.
        destruct (ltb_cantor_trichotomy b1 c1) as [|[|]] ; try now rewrite H2 in *.
        apply eqb_cantor_eq in H2.
        subst.
        simpl.
        rewrite ltb_cantor_irefl, Heqb.
        apply eq_cantor_inj ; split ; try easy ; apply eq_cantor_inj ; split ; try easy.
        apply IHb2 ; try assumption.
        now inversion H0.
        now inversion H1.
      + destruct (b1 <? a1) as []eqn:?.
        * destruct (b1 <? c1) as []eqn:?.
          -- destruct (a1 <? c1) as []eqn:?.
             ++ simpl.
                rewrite Heqb2.
                apply eq_cantor_inj ; split ; try easy.
                apply IHc2 in H as ? ; try now inversion H1.
                simpl in H2.
                rewrite Heqb, Heqb0 in H2.
                rewrite <- H2.
                now destruct c2.
             ++ destruct (c1 <? a1) as []eqn:? ; simpl ; rewrite Heqb2, Heqb3 ; apply eq_cantor_inj.
                ** split ; try reflexivity.
                   rewrite IHa2 ; try now inversion H.
                   simpl.
                   now rewrite Heqb1.
                ** split ; try reflexivity ; apply eq_cantor_inj ; split ; try reflexivity.
                   rewrite IHa2 ; try easy.
                   now inversion H.
                   now inversion H1.
          -- destruct (c1 <? b1) as []eqn:? ; rewrite Heqb, Heqb0 ; simpl.
             ** destruct (a1 <? c1) as []eqn:?.
                --- apply (ltb_cantor_trans c1 b1 a1) in Heqb2 as ? ; try assumption.
                    apply ltb_cantor_antisym in H2.
                    destruct H2 as [_ H2].
                    now rewrite H2 in Heqb3.
                --- apply (ltb_cantor_trans c1 b1 a1) in Heqb2 as ? ; try assumption.
                    rewrite H2.
                    apply eq_cantor_inj ; split ; try easy.
                    assert (ω^b1 +: merge_cantor b2 (ω^c1 +: c2) = merge_cantor (ω^b1 +: b2) (ω^c1 +: c2)) by (simpl ; now rewrite Heqb1, Heqb2).
                    rewrite H3.
                    apply IHa2 ; try assumption.
                    now inversion H.
             ** destruct (a1 <? c1) as []eqn:? ; destruct (ltb_cantor_trichotomy b1 c1) as [|[|]] ; try now rewrite H2 in *.
                --- apply eqb_cantor_eq in H2.
                    subst.
                    now rewrite Heqb3 in Heqb.
                --- apply eqb_cantor_eq in H2.
                    subst.
                    rewrite Heqb0.
                    apply eq_cantor_inj ; split ; try easy.
                    assert (ω^c1 +: (ω^c1 +: (merge_cantor b2 c2)) = merge_cantor (ω^c1 +: b2) (ω^c1 +: c2)) by (simpl ; now rewrite Heqb1).
                    rewrite H2.
                    apply IHa2 ; try assumption.
                    now inversion H.
        * destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H2 in *.
          apply eqb_cantor_eq in H2.
          subst.
          simpl.
          destruct (b1 <? c1) as []eqn:?.
          -- rewrite Heqb1.
             apply eq_cantor_inj ; split ; try easy.
             apply IHc2 in H as ? ; try assumption.
             simpl in H2.
             rewrite ltb_cantor_irefl in H2.
             now rewrite <- H2.
             now inversion H1.
          -- destruct (c1 <? b1) as []eqn:?.
             ++ rewrite ltb_cantor_irefl.
                apply eq_cantor_inj ; split ; try easy ; apply eq_cantor_inj ; split ; try easy.
                apply IHa2 ; try easy.
                now inversion H.
                now inversion H0.
             ++ rewrite ltb_cantor_irefl.
                destruct (ltb_cantor_trichotomy b1 c1) as [|[|]] ; try now rewrite H2 in *.
                apply eqb_cantor_eq in H2.
                subst.
                assert (merge_cantor a2 (ω^c1 +: merge_cantor b2 c2) = ω^c1 +: merge_cantor a2 (merge_cantor b2 c2)).
                  apply merge_cantor_normal_cons_r.
                  constructor.
                  now inversion H1.
                  apply merge_cantor_normal.
                  now inversion H0.
                  now inversion H1.
                  rewrite merge_cantor_max_degree.
                  apply leb_cantor_max_l.
                  split.
                  now inversion H0.
                  now inversion H1.
                  now inversion H.
                  now inversion H.
                assert (merge_cantor (ω^c1 +: merge_cantor a2 b2) c2 = ω^c1 +: merge_cantor (merge_cantor a2 b2) c2).
                  apply merge_cantor_normal_cons_l.
                  constructor.
                  now inversion H1.
                  apply merge_cantor_normal.
                  now inversion H.
                  now inversion H0.
                  rewrite merge_cantor_max_degree.
                  apply leb_cantor_max_l.
                  split.
                  now inversion H.
                  now inversion H0.
                  now inversion H1.
                  now inversion H1.
                rewrite H2.
                destruct c2.
                ** now rewrite merge_cantor_zero_r.
                ** simpl in H3.
                   rewrite H3, IHa2.
                   reflexivity.
                   now inversion H.
                   now inversion H0.
                   now inversion H1.
Qed.

Lemma add_cantor_max_degree : forall a b, degree (a + b) = max_cantor (degree a) (degree b).
Proof.
  destruct a.
  - intros.
    simpl.
    now destruct (degree b).
  - destruct b.
    + simpl.
      now destruct a1.
    + simpl.
      destruct (a1 <? b1) as []eqn:? ; destruct (b1 <? a1) ; unfold max_cantor ; now rewrite Heqb.
Qed.

Lemma add_cantor_normal : forall a b, normal a -> normal b -> normal (a + b).
Proof.
  induction a.
  - now intros.
  - destruct b ; intros.
    + assumption.
    + simpl.
      destruct (a1 <? b1) as []eqn:? ; destruct (b1 <? a1) as []eqn:?.
      * assumption.
      * assumption.
      * constructor.
        -- now inversion H.
        -- apply IHa2.
           now inversion H.
           assumption.
        -- rewrite add_cantor_max_degree.
           apply leb_cantor_max_l.
           split.
           ++ now inversion H.
           ++ now apply ltb_cantor_leb_incl.
      * constructor.
        -- now inversion H.
        -- constructor.
           ++ now inversion H0.
           ++ apply IHa2.
              ** now inversion H.
              ** now inversion H0.
           ++ rewrite add_cantor_max_degree.
              apply leb_cantor_max_l.
              split.
              ** destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H1 in *.
                 apply eqb_cantor_eq in H1.
                 subst.
                 now inversion H.
              ** now inversion H0.
        -- destruct (ltb_cantor_trichotomy a1 b1) as [|[|]] ; try now rewrite H1 in *.
           apply eqb_cantor_eq in H1.
           subst.
           unfold le_cantor.
           now rewrite leb_cantor_refl.
Qed.

Lemma sub_cantor_normal : forall a b, normal a -> normal (a - b).
Proof.
  intro a.
  induction a ; intros b N.
  - destruct b ; constructor.
  - destruct b.
    + assumption.
    + simpl.
      destruct (a1 <? b1) ; [| destruct (a1 =? b1) ; [ destruct (a2 <? b2) ; [| destruct (b1 <? a1) ] | destruct (b1 <? a1) ] ].
      * constructor.
      * constructor.
      * assumption.
      * apply IHa2.
        now inversion N.
      * assumption.
      * apply IHa2.
        now inversion N.
Qed.

Lemma remove_cantor_leb_degree : forall a b, normal a -> degree (a -- b) <=? degree a = true.
Proof.
  induction a ; intros b N.
  - now rewrite remove_cantor_zero_l, leb_cantor_refl.
  - simpl.
    destruct b.
    + now rewrite leb_cantor_refl.
    + destruct (a1 <? b1) ; [| destruct (a1 =? b1) ; [ destruct (a2 <? b2) ; [| destruct (b1 <? a1) ] | destruct (b1 <? a1) ] ].
      * now rewrite leb_cantor_zero_l.
      * now rewrite leb_cantor_zero_l.
      * now rewrite leb_cantor_refl.
      * apply leb_cantor_trans with (degree a2).
        -- apply IHa2.
           now inversion N.
        -- now inversion N.
      * now rewrite leb_cantor_refl.
      * apply leb_cantor_trans with (degree a2).
        -- apply IHa2.
           now inversion N.
        -- now inversion N.
Qed.

Lemma remove_cantor_normal : forall a b, normal a -> normal (a -- b).
Proof.
  intro a.
  induction a ; intros b N.
  - destruct b ; constructor.
  - destruct b.
    + assumption.
    + simpl.
      destruct (a1 <? b1) ; [| destruct (a1 =? b1) ; [ destruct (a2 <? b2) ; [| destruct (b1 <? a1) ] | destruct (b1 <? a1) ] ].
      * constructor.
      * constructor.
      * constructor ; inversion_clear N ; try assumption.
        -- now apply IHa2.
        -- apply leb_cantor_trans with (degree a2) ; try assumption.
           now apply remove_cantor_leb_degree.
      * apply IHa2.
        now inversion N.
      * constructor ; inversion_clear N ; try assumption.
        -- now apply IHa2.
        -- apply leb_cantor_trans with (degree a2) ; try assumption.
           now apply remove_cantor_leb_degree.
      * apply IHa2.
        now inversion N.
Qed.

Definition merge_cantorN a b :=
  match a, b with
  | Normal a Na, Normal b Nb => Normal (merge_cantor a b) (merge_cantor_normal a b Na Nb)
  end.

Lemma ltb_cantor_cons_pow : forall a b c, a <=? b = true -> degree b <? c = true -> a <? (ω^c +: b) = true.
Proof.
  destruct b ; intros.
  - apply leb_cantor_zero_r in H.
    now subst.
  - destruct a.
    + easy.
    + simpl in *.
      unfold leb_cantor in H.
      destruct (ltb_cantor (ω^a1 +: a2) (ω^b1 +: b2)) as []eqn:?.
      * simpl in Heqb.
        destruct (ltb_cantor a1 b1) as []eqn:?.
        -- now rewrite (ltb_cantor_trans a1 b1 c).
        -- destruct (eqb_cantor a1 b1) as []eqn:?.
           ++ apply eqb_cantor_eq in Heqb1.
              subst.
              now rewrite H0.
           ++ easy.
      * destruct (eqb_cantor (ω^a1 +: a2) (ω^b1 +: b2)) as []eqn:?.
        -- apply eqb_cantor_eq in Heqb0.
           injection Heqb0 ; intros.
           subst.
           now rewrite H0.
        -- easy.
Qed.

Lemma ltb_cantor_cons_pow_normal : forall a b c, normal b -> a <=? b = true -> degree b <=? c = true -> a <? ω^c +: b = true.
Proof.
  intros a b c N.
  revert a c.
  induction b ; intros.
  apply leb_cantor_zero_r in H.
  now subst.
  destruct a.
  easy.
  simpl.
  unfold leb_cantor in H, H0.
  simpl in H , H0.
  destruct (ltb_cantor a1 b1) as []eqn:?.
  destruct (ltb_cantor b1 c) as []eqn:?.
  now rewrite (ltb_cantor_trans a1 b1 c).
  simpl in H0.
  apply eqb_cantor_eq in H0.
  subst.
  now rewrite Heqb.
  destruct (eqb_cantor a1 b1) as []eqn:?.
  apply eqb_cantor_eq in Heqb0.
  subst.
  destruct (ltb_cantor b1 c) as []eqn:?.
  reflexivity.
  destruct (eqb_cantor b1 c) as []eqn:?.
  destruct (ltb_cantor a2 b2) as []eqn:?.
  apply IHb2.
  now inversion N.
  unfold leb_cantor.
  now rewrite Heqb2.
  now inversion N.
  simpl in H.
  apply IHb2.
  now inversion N.
  unfold leb_cantor.
  now rewrite Heqb2.
  now inversion N.
  easy.
  easy.
Qed.

Lemma leb_cantor_eq_pow : forall a b c, ω^a +: b <=? ω^a +: c = true <-> b <=? c = true.
Proof.
  intros ; split ; intro.
  unfold leb_cantor in H.
  destruct ((ω^a +: b <? ω^a +: c)) as []eqn:?.
  simpl in Heqb0.
  rewrite ltb_cantor_irefl, eqb_cantor_refl in Heqb0.
  unfold leb_cantor.
  now rewrite Heqb0.
  destruct ((ω^a +: b =? ω^a +: c)) as []eqn:?.
  apply eqb_cantor_eq in Heqb1.
  apply eq_cantor_inj in Heqb1.
  destruct Heqb1 as [_].
  subst.
  now apply leb_cantor_refl.
  easy.
  unfold leb_cantor in *.
  simpl.
  rewrite ltb_cantor_irefl, eqb_cantor_refl.
  destruct (b <? c) as []eqn:?.
  easy.
  now destruct (b =? c) as []eqn:?.
Qed.

Lemma add_cantor_zero_r : forall a, a + zero = a.
Proof.
  now destruct a.
Qed.

Lemma add_cantor_zero_l : forall a, zero + a = a.
Proof.
  now destruct a.
Qed.

Lemma add_cantor_zero : forall a b, add_cantor a b = zero -> a = zero /\ b = zero.
Proof.
  intros.
  split.
  destruct b.
  now rewrite add_cantor_zero_r in H.
  destruct a.
  now rewrite add_cantor_zero_l in H.
  simpl in H.
  now destruct (a1 <? b1) ; destruct (b1 <? a1).
  destruct a.
  now rewrite add_cantor_zero_l in H.
  destruct b.
  now rewrite add_cantor_zero_r in H.
  simpl in H.
  now destruct (a1 <? b1) ; destruct (b1 <? a1).
Qed.

Lemma ltb_leb_add_normal : forall a b, normal a -> normal b -> (ltb_cantor a b = true <-> leb_cantor (a + (ω^zero +: zero)) b = true).
Proof.
  induction a.
  intros.
  split ; intros.
  simpl.
  destruct (ltb_cantor_trichotomy (ω^zero +: zero) b) as [|[|]].
  unfold leb_cantor.
  now rewrite H2.
  unfold leb_cantor.
  rewrite H2.
  now destruct (ltb_cantor (ω^zero +: zero) b).
  destruct b.
  easy.
  simpl in H2.
  repeat rewrite ltb_cantor_zero_r in H2.
  now destruct (eqb_cantor b1 zero).
  simpl in H1.
  assert (ltb_cantor zero (ω^zero +: zero) = true) by easy.
  now apply ltb_leb_cantor_trans with (ω^zero +: zero).
  induction b ; intros ; split ; intros.
  now rewrite ltb_cantor_zero_r in H1.
  apply leb_cantor_zero_r in H1.
  simpl in H1.
  rewrite ltb_cantor_zero_r in H1.
  now destruct a1.
  simpl.
  rewrite ltb_cantor_zero_r.
  destruct a1.
  destruct b1.
  simpl in H1.
  rewrite add_cantor_zero_r.
  apply IHa2 in H1 as ?.
  assert (forall c, normal c -> degree c = zero -> ω^zero +: c = c + (ω^zero +: zero)).
  induction c ; intros.
  easy.
  simpl in H4.
  subst.
  apply eq_cantor_inj ; split ; try reflexivity.
  now rewrite add_cantor_zero_r.
  apply leb_cantor_eq_pow.
  rewrite (H3 a2).
  assumption.
  now inversion H.
  inversion_clear H.
  now apply leb_cantor_zero_r in H6.
  now inversion H.
  now inversion H0.
  easy.
  simpl in H1.
  destruct b1.
  easy.
  destruct (a1_1 <? b1_1) as []eqn:?.
  unfold leb_cantor.
  simpl.
  now rewrite Heqb.
  destruct (a1_1 =? b1_1) as []eqn:?.
  apply eqb_cantor_eq in Heqb0.
  subst.
  destruct (a1_2 <? b1_2) as []eqn:?.
  unfold leb_cantor.
  simpl.
  now rewrite Heqb, Heqb0, eqb_cantor_refl.
  destruct (a1_2 =? b1_2) as []eqn:?.
  simpl in H1.
  unfold leb_cantor.
  simpl.
  rewrite Heqb, Heqb0, Heqb1, eqb_cantor_refl.
  simpl.
  apply IHa2.
  now inversion H.
  now inversion H0.
  assumption.
  easy.
  easy.
  apply ltb_leb_cantor_trans with (ω^a1 +: a2 + (ω^zero +: zero)) ; try assumption.
  assert (forall c, normal c -> c <? c + (ω^zero +: zero) = true).
  induction c ; intro.
  easy.
  simpl.
  rewrite ltb_cantor_zero_r.
  destruct c1.
  rewrite ltb_cantor_zero_r, eqb_cantor_refl.
  rewrite add_cantor_zero_r.
  assert (ω^zero +: c2 = c2 + (ω^zero +: zero)).
  induction c2.
  easy.
  simpl.
  rewrite ltb_cantor_zero_r.
  inversion_clear H2.
  simpl in H5.
  apply leb_cantor_zero_r in H5.
  rewrite H5.
  apply eq_cantor_inj ; split ; try reflexivity.
  now rewrite add_cantor_zero_r.
  rewrite H3.
  apply IHc2.
  now inversion H2.
  rewrite ltb_cantor_irefl, eqb_cantor_refl.
  apply IHc2.
  now inversion H2.
  now apply H2.
Qed.

Lemma sub_cantor_leb : forall a b, normal a -> a - b <=? a = true.
Proof.
  intro a.
  induction a ; intros b N.
  - destruct b ; now rewrite leb_cantor_refl.
  - destruct b.
    + now rewrite leb_cantor_refl.
    + simpl.
      destruct (a1 <? b1) ; [| destruct (a1 =? b1) ; [ destruct (a2 <? b2) ; [| destruct (b1 <? a1) ] | destruct (b1 <? a1) ] ].
      * now rewrite leb_cantor_zero_l.
      * now rewrite leb_cantor_zero_l.
      * now rewrite leb_cantor_refl.
      * apply leb_cantor_trans with a2.
        -- apply IHa2.
           now inversion N.
        -- apply ltb_cantor_leb_incl, ltb_cantor_cons_pow_normal ; try now inversion N.
           now rewrite leb_cantor_refl.
      * now rewrite leb_cantor_refl.
      * apply leb_cantor_trans with a2.
        -- apply IHa2.
           now inversion N.
        -- apply ltb_cantor_leb_incl, ltb_cantor_cons_pow_normal ; try now inversion N.
           now rewrite leb_cantor_refl.
Qed.

Lemma remove_cantor_leb : forall a b, normal a -> a -- b <=? a = true.
Proof.
  intro a.
  induction a ; intros b N.
  - destruct b ; now rewrite leb_cantor_refl.
  - destruct b.
    + now rewrite leb_cantor_refl.
    + simpl.
      destruct (a1 <? b1) ; [| destruct (a1 =? b1) ; [ destruct (a2 <? b2) ; [| destruct (b1 <? a1) ] | destruct (b1 <? a1) ] ].
      * now rewrite leb_cantor_zero_l.
      * now rewrite leb_cantor_zero_l.
      * inversion_clear N.
        apply (IHa2 (ω^b1 +: b2)) in H0 as H2.
        unfold leb_cantor in H2.
        destruct (a2 -- ω^ b1 +: b2 <? a2)eqn:Heqb ; unfold leb_cantor.
        -- cut (ω^a1 +: (a2 -- ω^b1 +: b2) <? ω^a1 +: a2 = true).
             intro H3.
             now rewrite H3.
           simpl.
           now rewrite ltb_cantor_irefl, eqb_cantor_refl.
        -- simpl in H2.
           cut (ω^a1 +: (a2 -- ω^b1 +: b2) =? ω^a1 +: a2 = true).
             intro H3.
             rewrite H3.
             now destruct (ω^a1 +: (a2 -- ω^b1 +: b2) <? ω^a1 +: a2).
           simpl.
           now rewrite eqb_cantor_refl, H2.
      * apply leb_cantor_trans with a2.
        -- apply IHa2.
           now inversion N.
        -- apply ltb_cantor_leb_incl, ltb_cantor_cons_pow_normal ; try now inversion N.
           now rewrite leb_cantor_refl.
      * inversion_clear N.
        apply (IHa2 (ω^b1 +: b2)) in H0 as H2.
        unfold leb_cantor in H2.
        destruct (a2 -- ω^ b1 +: b2 <? a2)eqn:Heqb ; unfold leb_cantor.
        -- cut (ω^a1 +: (a2 -- ω^b1 +: b2) <? ω^a1 +: a2 = true).
             intro H3.
             now rewrite H3.
           simpl.
           now rewrite ltb_cantor_irefl, eqb_cantor_refl.
        -- simpl in H2.
           cut (ω^a1 +: (a2 -- ω^b1 +: b2) =? ω^a1 +: a2 = true).
             intro H3.
             rewrite H3.
             now destruct (ω^a1 +: (a2 -- ω^b1 +: b2) <? ω^a1 +: a2).
           simpl.
           now rewrite eqb_cantor_refl, H2.
      * apply leb_cantor_trans with a2.
        -- apply IHa2.
           now inversion N.
        -- apply ltb_cantor_leb_incl, ltb_cantor_cons_pow_normal ; try now inversion N.
           now rewrite leb_cantor_refl.
Qed.

(*
Fact subε0Ord_cantor : forall a, Ord.subε0Ord a -> exists b, Ord.eq_ord a (ord_of_cantor b).
Proof.
  induction 1.
  - exists zero.
    now apply Ord.eq_eq_ord_inc.
  - exists (ω^ω^zero +: zero +: zero).
    simpl.
     split ; apply Ord.le_ord_Limit ; intro n ; apply Ord.le_ord_limit_r with n ; induction n.
     + now apply Ord.le_ord_refl.
     + now apply Ord.le_ord_succ_mono.
     + now apply Ord.le_ord_refl.
     + now apply Ord.le_ord_succ_mono.
  - destruct IHsubε0Ord as [b H0].
    exists (b + (ω^zero +: zero)).
    split.
  simpl.
  apply Ord.le_ord_succ_mono.
  now destruct H0.
  simpl.
  apply Ord.le_ord_succ_mono.
  now destruct H0.
  simpl.
  rewrite ltb_cantor_zero_r.
  admit.
  admit.
  destruct IHsubε0Ord1 as [a' ?].
  destruct IHsubε0Ord2 as [b' ?].
  exists (add_cantor a' b') ; split.
Abort.
*)

Fixpoint cantor_of_nat n :=
  match n with
  | 0 => zero
  | S n => ω^zero +: (cantor_of_nat n)
  end.

Lemma cantor_of_nat_normal : forall n, normal (cantor_of_nat n).
Proof.
  induction n.
  constructor.
  repeat constructor.
  assumption.
  now destruct n.
Qed.

Definition normal_of_nat n := Normal (cantor_of_nat n) (cantor_of_nat_normal n).

Fixpoint scale a n :=
  match n with
  | 0 => zero
  | S n => ω^a +: scale a n
  end.

Lemma scale_normal : forall a n, normal a -> normal (scale a n).
Proof.
  intros a n.
  induction n as [| n IHn ] ; intro Na.
  - constructor.
  - simpl.
    constructor ; try assumption.
    + now apply IHn.
    + destruct n as [| n ].
      * apply leb_cantor_zero_l.
      * apply leb_cantor_refl.
Qed.

Lemma degree_zero_scale : forall a, normal a -> degree a = zero -> exists n, a = scale zero n.
Proof.
  induction a ; intros N H.
  - now exists 0.
  - simpl in H.
    subst.
    inversion_clear N.
    apply leb_cantor_zero_r in H1.
    destruct IHa2 as [n H2] ; try assumption.
    exists (S n).
    simpl.
    now apply eq_cantor_inj.
Qed.

(** Lemme de séparation d'un ordinal : un ordinal sous forme normale de Cantor s'écrit
    - soit ω#<sup>a</sup>#×n + ω#<sup>b</sup>#×m  + c avec b < a
    - soit ω#<sup>a</sup>#×n + 0

    la version ci-dessous prend en compte la convention [degree zero = zero] *)
Lemma dominant_part : forall a, normal a -> (exists b n, normal b /\ degree b <? degree a = true /\ merge_cantor (scale (degree a) n) b = a /\ n <> 0) \/ (exists n, a = scale zero n).
Proof.
  intros.
  destruct (ltb_cantor_trichotomy a (ω^ω^zero +: zero +: zero)) as [|[|]].
  - right.
    induction a.
    + now exists 0.
    + simpl in H0.
      destruct (a1 <? ω^zero +: zero) as []eqn:?.
      * inversion_clear H.
        assert (a1 = zero).
          destruct a1.
          easy.
          simpl in Heqb.
          repeat rewrite ltb_cantor_zero_r in Heqb.
          now destruct (a1_1 =? zero).
       subst.
       apply IHa2 in H2 as ?.
       destruct H as [n H].
       exists (S n).
       simpl.
       now rewrite H.
       destruct a2.
       -- easy.
       -- simpl.
          simpl in H3.
          apply leb_cantor_zero_r in H3.
          now rewrite H3.
      * rewrite ltb_cantor_zero_r in H0.
        now destruct (a1 =? _).
  - apply eqb_cantor_eq in H0.
    subst.
    left.
    exists zero, 1.
    repeat split.
    + constructor.
    + easy.
  - left.
    induction a.
    + now rewrite ltb_cantor_zero_r in H0.
    + inversion_clear H.
      destruct (ω^ω^zero +: zero +: zero <? a2) as []eqn:Heqa.
      * apply IHa2 in H2 as ?.
        destruct a2.
        -- simpl in H3.
           exists zero, 1.
           repeat split.
           ++ constructor.
           ++ now destruct a1.
           ++ easy.
        -- simpl in H3.
           unfold le_cantor, leb_cantor in H3.
           destruct (a2_1 <? a1) as []eqn:?.
           ++ exists (ω^a2_1 +: a2_2), 1.
              repeat split.
              ** assumption.
              ** assumption.
              ** simpl.
                destruct (a1 <? a2_1) as []eqn:?.
                --- apply (ltb_cantor_trans a1 a2_1 a1) in Heqb0 ; try assumption.
                    now rewrite ltb_cantor_irefl in Heqb0.
                --- now rewrite Heqb.
              ** easy.
           ++ simpl in H3.
              apply eqb_cantor_eq in H3.
              subst.
              destruct H as [b [n H]].
              exists b, (S n).
              destruct H as [? [? [? H5]]].
              repeat split ; try assumption.
              simpl in H4.
              simpl scale.
              rewrite merge_cantor_normal_cons_l ; try assumption.
              now rewrite H4.
              constructor ; try assumption.
              now apply scale_normal.
              destruct n.
              ** apply leb_cantor_zero_l.
              ** apply leb_cantor_refl.
              ** now apply ltb_cantor_leb_incl.
              ** easy.
        -- reflexivity.
      * pose proof (ltb_cantor_negb_leb (ω^ω^zero +: zero +: zero) a2).
        rewrite Heqa in H.
        destruct (a2 <=? ω^ω^zero +: zero +: zero) as []eqn:? ; try easy.
        unfold leb_cantor in Heqb.
        destruct (a2 <? ω^ω^zero +: zero +: zero) as []eqn:?.
        -- assert (degree a2 = zero).
             destruct a2.
             easy.
             simpl in Heqb0.
             destruct (a2_1 <? ω^zero +: zero) as []eqn:?.
             assert (a2_1 = zero).
               destruct a2_1.
               easy.
               simpl in Heqb1.
               repeat rewrite ltb_cantor_zero_r in Heqb1.
               now destruct (a2_1_1 =? zero).
             now subst.
             rewrite ltb_cantor_zero_r in Heqb0.
             now destruct (a2_1 =? ω^zero +: zero).
           exists a2, 1.
           repeat split.
           ++ assumption.
           ++ assert (ω^zero +: zero <=? a1 = true).
                simpl in H0.
                destruct a1.
                easy.
                destruct a1_1.
                now destruct a1_2.
                easy.
              rewrite H4.
              now apply ltb_leb_cantor_trans with (ω^zero +: zero).
           ++ simpl.
              destruct a2.
              ** easy.
              ** simpl in H3.
                 pose proof (ltb_cantor_negb_leb a1 a2_1).
                 rewrite H3 in H5.
                 simpl in H5.
                 rewrite H5.
                 destruct (a2_1 <? a1).
                 --- reflexivity.
                 --- now destruct a2_2.
           ++ easy.
        -- simpl in Heqb.
           apply eqb_cantor_eq in Heqb.
           subst.
           simpl in H3.
           unfold le_cantor, leb_cantor in H3.
           destruct (ω^zero +: zero <? a1) as []eqn:?.
           ++ exists (ω^ω^zero +: zero +: zero), 1.
              repeat split.
              ** easy.
              ** assumption.
              ** simpl scale.
                 apply merge_cantor_normal_cons_l.
                 constructor.
                 assumption.
                 constructor.
                 now apply leb_cantor_zero_l.
                 easy.
                 now apply ltb_cantor_leb_incl.
              ** easy.
           ++ unfold orb in H3.
              apply eqb_cantor_eq in H3.
              subst.
              exists zero, 2.
              repeat split.
              ** constructor.
              ** easy.
Qed.

Lemma dominant_part_uniq : forall a n m, normal a -> (exists b, normal b /\ degree b <? degree a = true /\ merge_cantor (scale (degree a) n) b = a /\ n <> 0) -> (exists c, normal c /\ degree c <? degree a = true /\ merge_cantor (scale (degree a) m) c = a /\ m <> 0) -> n = m.
Proof.
  intros a n.
  revert a.
  induction n ; intros a m N H H0.
  - now destruct H as [b [? [? []]]].
  - destruct H as [b [? [? []]]].
    destruct H0 as [c [? [? []]]].
    simpl scale in H2.
    rewrite merge_cantor_normal_cons_l in H2.
    assert (merge_cantor (scale (degree a) n) b = a -- ω^degree a +: zero).
      rewrite <- H2 at 2.
      simpl.
      now rewrite ltb_cantor_irefl, eqb_cantor_refl, ltb_cantor_zero_r, remove_cantor_zero_r.
    destruct m ; try easy.
    simpl scale in H5.
    rewrite merge_cantor_normal_cons_l in H5.
    assert (merge_cantor (scale (degree a) m) c = a -- ω^degree a +: zero).
      rewrite <- H5 at 2.
      simpl.
      now rewrite ltb_cantor_irefl, eqb_cantor_refl, ltb_cantor_zero_r, remove_cantor_zero_r.
    destruct n.
    destruct m ; simpl scale in * ; rewrite merge_cantor_zero_l in *.
    reflexivity.
    rewrite <- H7 in H8.
    rewrite merge_cantor_normal_cons_l in H8.
    assert (degree b = degree a) by (now rewrite <- H8).
    apply ltb_cantor_antisym in H1.
    destruct H1 as [H1 _].
    now apply neqb_cantor_neq in H1.
    constructor.
    now apply normal_degree.
    now apply scale_normal, normal_degree.
    destruct m.
    unfold le_cantor.
    now rewrite leb_cantor_zero_l.
    simpl.
    unfold le_cantor.
    now rewrite leb_cantor_refl.
    assumption.
    unfold le_cantor.
    now apply ltb_cantor_leb_incl.
    destruct m.
    simpl scale in *.
    rewrite merge_cantor_zero_l in *.
    rewrite <- H8 in H7.
    rewrite merge_cantor_normal_cons_l in H7.
    assert (degree c = degree a) by (now rewrite <- H7).
    apply ltb_cantor_antisym in H4.
    destruct H4 as [H4 _].
    now apply neqb_cantor_neq in H4.
    constructor.
    now apply normal_degree.
    now apply scale_normal, normal_degree.
    destruct n.
    unfold le_cantor.
    now rewrite leb_cantor_zero_l.
    simpl.
    unfold le_cantor.
    now rewrite leb_cantor_refl.
    assumption.
    unfold le_cantor.
    now apply ltb_cantor_leb_incl.
    assert (degree (a -- ω^degree a +: zero) = degree a).
      simpl scale in H7.
      rewrite merge_cantor_normal_cons_l in H7.
      now rewrite <- H7.
      constructor.
      now apply normal_degree.
      now apply scale_normal, normal_degree.
      destruct n.
      unfold le_cantor.
      now rewrite leb_cantor_zero_l.
      simpl.
      unfold le_cantor.
      now rewrite leb_cantor_refl.
      assumption.
      unfold le_cantor.
      now apply ltb_cantor_leb_incl.
    rewrite (IHn (a -- ω^degree a +: zero) (S m)).
    reflexivity.
    now apply remove_cantor_normal.
    exists b.
    repeat split.
    assumption.
    now rewrite H9.
    now rewrite H9.
    easy.
    exists c.
    repeat split.
    assumption.
    now rewrite H9.
    now rewrite H9.
    easy.
    constructor.
    now apply normal_degree.
    now apply scale_normal, normal_degree.
    destruct m.
    unfold le_cantor.
    now rewrite leb_cantor_zero_l.
    simpl.
    unfold le_cantor.
    now rewrite leb_cantor_refl.
    assumption.
    unfold le_cantor.
    now apply ltb_cantor_leb_incl.
    constructor.
    now apply normal_degree.
    now apply scale_normal, normal_degree.
    destruct n.
    unfold le_cantor.
    now rewrite leb_cantor_zero_l.
    simpl.
    unfold le_cantor.
    now rewrite leb_cantor_refl.
    assumption.
    unfold le_cantor.
    now apply ltb_cantor_leb_incl.
Qed.

(** Version des propriétés d'accessibilité et de bonne fondaison sur les éléments de type [A] vérifiant le prédicat [P] *)
Inductive AccP {A : Type} (P : A -> Prop) (R : A -> A -> Prop) (a : A) : Prop :=
  AccP_intro : P a -> (forall b : A, P b -> R b a -> AccP P R b) -> AccP P R a.

Definition well_foundedP {A : Type} (P : A -> Prop) (R : A -> A -> Prop) := forall a : A, P a -> AccP P R a.

Lemma well_foundedP_cantor : well_foundedP normal lt_cantor.
Proof.
  unfold well_foundedP.
  induction a ; intros.
  - apply AccP_intro.
    constructor.
    intros.
    unfold lt_cantor in H1.
    now rewrite ltb_cantor_zero_r in H1.
  - inversion_clear H.
    revert H1 H2.
    clear IHa2.
    revert a2.
    apply IHa1 in H0.
    clear IHa1.
    (* On procède par induction sur l'hypothèse H0 *)
    induction H0 as [a1 Na1 IH0 IH1].
    intros a2 Na2 Na.
    (* On prouve la propriété suivante *)
    assert (forall c, AccP normal lt_cantor c -> normal (ω^a1 +: c) -> AccP normal lt_cantor (ω^a1 +: c)) as IH2.
      intros.
      induction H as [c].
      apply AccP_intro ; try assumption.
      intros d ? ?.
      destruct d.
      apply AccP_intro.
      constructor.
      intros.
      unfold lt_cantor in H6.
      now rewrite ltb_cantor_zero_r in H6.
      unfold lt_cantor in H4.
      simpl in H4.
      destruct (d1 <? a1) as []eqn:?.
      inversion_clear H3.
      pose proof (IH1 d1 H5 Heqb d2).
      now apply H3 in H6.
      destruct (d1 =? a1) as []eqn:?.
      apply eqb_cantor_eq in Heqb0.
      subst.
      apply H2 ; try easy.
      now inversion H3.
      easy.
    apply AccP_intro.
    now constructor.
    intros b Nb H.
    destruct b.
    + apply AccP_intro.
      constructor.
      intros.
      unfold lt_cantor in H1.
      now rewrite ltb_cantor_zero_r in H1.
    + unfold lt_cantor in H.
      simpl in H.
      destruct (b1 <? a1) as []eqn:? ; [| destruct (b1 =? a1) as []eqn:? ; [| easy ] ].
      * inversion_clear Nb.
        now apply IH1.
      * apply eqb_cantor_eq in Heqb0.
        subst.
        (* On utilise le lemme de séparation de la partie dominante *)
        destruct (dominant_part (ω^a1 +: b2) Nb).
        -- destruct H0 as [b [n [H0 [H1 [H2 _]]]]].
           rewrite <- H2.
           destruct b ; simpl ; clear H2.
           ++ rewrite merge_cantor_zero_r.
              induction n.
              ** apply AccP_intro.
                 constructor.
                 intros.
                 unfold lt_cantor in H3.
                 now rewrite ltb_cantor_zero_r in H3.
              ** simpl.
                 apply IH2 ; try assumption.
                 constructor ; try assumption.
                 now apply scale_normal.
                 destruct n.
                 --- apply leb_cantor_zero_l.
                 --- apply leb_cantor_refl.
           ++ induction n.
              ** rewrite merge_cantor_zero_l.
                 apply IH1 ; now inversion H0.
              ** simpl.
                 simpl in H1.
                 pose proof (ltb_cantor_negb_leb a1 b1).
                 apply ltb_cantor_leb_incl in H1 as ?.
                 rewrite H3 in H2.
                 simpl in H2.
                 rewrite H2, H1.
                 apply IH2 ; try assumption.
                 constructor ; try assumption.
                 apply merge_cantor_normal.
                 now apply scale_normal.
                 assumption.
                 rewrite merge_cantor_max_degree.
                 destruct n ; simpl ; apply leb_cantor_max_l.
                 --- split ; try assumption.
                     apply leb_cantor_zero_l.
                 --- split ; try assumption.
                     apply leb_cantor_refl.
        -- destruct H0 as [n H0].
           rewrite H0.
           assert (a1 = zero).
             destruct n.
             easy.
             simpl in H0.
             now injection H0.
           clear H0.
           subst.
           induction n.
           ++ apply AccP_intro.
              constructor.
              intros.
              unfold lt_cantor in H1.
              now rewrite ltb_cantor_zero_r in H1.
           ++ simpl.
              apply IH2 ; try assumption.
              repeat constructor.
              apply scale_normal.
              constructor.
              destruct n ; constructor.
Qed.

(** [<] est une relation d'ordre bien fondée pour les ordinaux en forme normale *)
Theorem well_founded_cantor : well_founded lt_cantorN.
Proof.
  unfold well_founded.
  intro a.
  destruct a as [a Na].
  pose proof (well_foundedP_cantor a Na).
  induction H.
  apply Acc_intro.
  intros [b Nb] ?.
  apply H1 ; now unfold lt_cantorN, lt_cantor in H2.
Qed.

(** Récurrence tranfinie jusqu'à ε#<sub>0</sub># *)
Theorem transfinite_ind (P : CantorNormalOrd -> Prop) :
  (forall a, (forall b, lt_cantorN b a -> P b) -> P a) ->
  forall a, P a.
Proof.
  intros.
  now apply (well_founded_ind well_founded_cantor).
Qed.


Theorem well_foundedP_induction_type {A : Type} (R : A -> A -> Prop) (P' : A -> Prop) (wf : well_foundedP P' R) :
  forall P:A -> Type,
  (forall x:A, P' x -> (forall y:A, P' y -> R y x -> P y) -> P x) -> forall a:A, P' a -> P a.
Proof.
  intros ; apply AccP_rect with P' R ; auto.
Defined.

Theorem transfinite_indP (P : CantorOrd -> Prop) :
  (forall a, normal a -> (forall b, normal b -> b <? a = true -> P b) -> P a) ->
  forall a, normal a -> P a.
Proof.
  now apply (well_foundedP_induction_type lt_cantor normal well_foundedP_cantor).
Qed.

(* Un ordinal de type CantorOrd peut être vu comme une étape de calcul d'un ordinal de type CantorNormalOrd *)
Fixpoint simplify_cantor a :=
  match a with
  | zero => zero
  | ω^a' +: a'' => (ω^(simplify_cantor a') +: zero) + (simplify_cantor a'')
  end.

Lemma simplify_normal : forall a, normal (simplify_cantor a).
Proof.
  induction a.
  - constructor.
  - simpl.
    destruct (simplify_cantor a2).
    + constructor.
      * assumption.
      * assumption.
      * unfold le_cantor.
        now rewrite leb_cantor_zero_l.
    + destruct (simplify_cantor a1 <? c1) as []eqn:?.
      * assumption.
      * destruct (c1 <? simplify_cantor a1) as []eqn:?.
        -- constructor.
           ++ assumption.
           ++ assumption.
           ++ now apply ltb_cantor_leb_incl.
        -- constructor.
           ++ assumption.
           ++ assumption.
           ++ destruct (ltb_cantor_trichotomy (simplify_cantor a1) c1) as [H|[H|H]] ; try now rewrite H in *.
              apply eqb_cantor_eq in H.
              rewrite H.
              unfold le_cantor.
              now rewrite leb_cantor_refl.
Qed.

Lemma normal_simplify : forall a, normal a -> simplify_cantor a = a.
Proof.
  induction a ; intro.
  - reflexivity.
  - inversion_clear H.
    simpl.
    rewrite IHa1 ; try assumption.
    rewrite IHa2 ; try assumption.
    destruct a2.
    + reflexivity.
    + simpl in H2.
      unfold le_cantor, leb_cantor in H2.
      destruct (a2_1 <? a1) as []eqn:?.
      * apply ltb_cantor_antisym in Heqb.
        destruct Heqb.
        now rewrite H3.
      * simpl in H2.
        apply eqb_cantor_eq in H2.
        subst.
        now rewrite ltb_cantor_irefl.
Qed.

Definition simplify_cantorN a := Normal (simplify_cantor a) (simplify_normal a).

(* Un ordinal de type CantorOrd peut être vu comme un ordinal de type CantorNormalOrd dont les termes sont donnés comme un ensemble *)
Fixpoint normalize a :=
  match a with
  | zero => zero
  | ω^a' +: a'' => merge_cantor (ω^(normalize a') +: zero) (normalize a'')
  end.

Lemma normalize_normal : forall a, normal (normalize a).
Proof.
  induction a.
  constructor.
  simpl.
  assert (normal (ω^(normalize a1) +: zero)).
  constructor.
  assumption.
  constructor.
  now apply leb_cantor_zero_l.
  apply (merge_cantor_normal (ω^(normalize a1) +: zero) (normalize a2)) in H.
  assumption.
  assumption.
Qed.

Lemma normal_normalize : forall a, normal a -> normalize a = a.
Proof.
  induction a ; intros.
  easy.
  simpl.
  inversion_clear H.
  rewrite IHa1 ; try assumption.
  rewrite IHa2 ; try assumption.
  destruct a2.
  reflexivity.
  simpl in H2.
  pose proof (ltb_cantor_negb_leb a1 a2_1).
  rewrite H2 in H.
  simpl in H.
  rewrite H.
  destruct (a2_1 <? a1).
  reflexivity.
  now destruct a2_2.
Qed.

Definition normalizeN a := Normal (normalize a) (normalize_normal a).

Fixpoint length a :=
  match a with
  | zero => 0
  | ω^_ +: a => S (length a)
  end.

Definition lengthN a := match a with Normal a _ => length a end.

Fixpoint kth k (a : CantorOrd) (default : CantorOrd) :=
  match a with
  | zero => default
  | ω^a' +: a'' =>
    match k with
    | 0 => a'
    | S k => kth k a'' default
    end
  end.

Lemma kth_normal_length : forall k a default, normal a -> (k < length a)%nat -> normal (kth k a default).
Proof.
  induction k ; intros ; destruct a.
  easy.
  now inversion H.
  easy.
  apply IHk.
  now inversion H.
  now apply le_S_n.
Qed.

Lemma kth_normal_default : forall k (a default : CantorOrd), normal a -> normal default -> normal (kth k a default).
Proof.
  induction k ; intros ; destruct a.
  easy.
  now inversion H.
  easy.
  apply IHk.
  now inversion H.
  assumption.
Qed.


Definition kthN k a default :=
  match a, default with
  | Normal a Na, Normal default Ndefault => Normal (kth k a default) (kth_normal_default k a default Na Ndefault)
  end.

Fact one_normal : normal (ω^zero +: zero).
Proof.
  repeat constructor.
Qed.

Definition oneN := Normal (ω^zero +: zero) one_normal.

Fact exp_normal : forall a, normal a -> normal (ω^a +: zero).
Proof.
  intros.
  constructor.
  assumption.
  constructor.
  now destruct a.
Qed.

Definition expN a :=
  match a with
  | Normal a Na => Normal (ω^a +: zero) (exp_normal a Na)
  end.

Fixpoint sub_exp a b :=
  match a with
  | zero => zero
  | ω^a' +: a'' =>
    if a' =? b
      then sub_exp a'' b
      else ω^a' +: (sub_exp a'' b)
  end.

Lemma sub_exp_leb : forall a b, normal a -> sub_exp a b <=? a = true.
Proof.
  induction a ; intros b N.
  - now rewrite leb_cantor_refl.
  - simpl.
    destruct (a1 =? b).
    + apply ltb_cantor_leb_incl.
      rewrite ltb_cantor_cons_pow_normal ; [| | apply IHa2 |] ; now inversion N.
    + unfold leb_cantor.
      simpl.
      rewrite ltb_cantor_irefl, eqb_cantor_refl.
      simpl.
      unfold leb_cantor in IHa2.
      rewrite IHa2 ; now inversion N.
Qed.

Lemma sub_exp_zero_scale : forall a b, sub_exp a b = zero <-> exists n, a = scale b n.
Proof.
  induction a ; intro ; split ; intro.
  - now exists 0.
  - easy.
  - simpl in H.
    destruct (a1 =? b)eqn:? ; [| easy ].
    apply eqb_cantor_eq in Heqb0.
    apply IHa2 in H.
    destruct H as [n H].
    exists (S n).
    simpl.
    now subst.
  - destruct H as [n H].
    assert (a1 = b).
      destruct n.
      easy.
      apply eq_cantor_inj in H.
      now destruct H.
    subst.
    simpl.
    rewrite eqb_cantor_refl.
    apply IHa2.
    destruct n.
    + easy.
    + exists n.
      simpl in H.
      apply eq_cantor_inj in H.
      now destruct H.
Qed.

Lemma leb_cantor_sub_exp_degree : forall a b, normal a -> degree (sub_exp a b) <=? degree a = true.
Proof.
  induction a ; intros b N.
  - now rewrite leb_cantor_refl.
  - simpl.
    destruct (a1 =? b).
    + inversion_clear N.
      apply leb_cantor_trans with (degree a2).
      * now apply IHa2.
      * assumption.
    + now rewrite leb_cantor_refl.
Qed.

Lemma sub_exp_degree_leb_cantor : forall a b, b =? degree a = false -> degree (sub_exp a b) = degree a.
Proof.
  induction a ; intros b H.
  - reflexivity.
  - simpl.
    simpl in H.
    rewrite eqb_cantor_sym in H.
    now rewrite H.
Qed.

Lemma sub_exp_normal : forall a b, normal a -> normal (sub_exp a b).
Proof.
  induction a ; intros b N.
  - assumption.
  - simpl.
    destruct (a1 =? b).
    + apply IHa2.
      now inversion N.
    + constructor.
      * now inversion N.
      * apply IHa2.
        now inversion N.
      * apply leb_cantor_trans with (degree a2) ; try now inversion N.
        apply leb_cantor_sub_exp_degree ; try now inversion N.
Qed.

Fixpoint concat_cantor a b :=
  match a with
  | zero => b
  | ω^a +: a' => ω^a +: (concat_cantor a' b)
  end.
Infix "++" := concat_cantor.

Lemma concat_assoc : forall a b c, (a ++ b) ++ c = a ++ b ++ c.
Proof.
  induction a ; intros b c.
  - reflexivity.
  - simpl.
    now rewrite IHa2.
Qed.

(** Le type [Hydra] est isomorphe au type [CantorOrd] *)
Definition Hydra := CantorOrd.

Fixpoint round' h h' :=
  match h, h' with
  | zero, _ => false
  | ω^zero +: zero, zero => true
  | _, zero => false
  | ω^a +: a', ω^b +: b' =>
    if a =? b
      then round' a' b' (* on a supprimé une autre tête *)
    else (if a' =? b'
      then round' a b (* on a supprimé une tête plus haute *)
    else (if (andb ((a -- ω^zero +: zero) <? a) (a' <? h'))
      then sub_exp a' (a -- ω^zero +: zero) =? sub_exp h' (a -- ω^zero +: zero)
      else false))
  end.

Definition round h h' := round' (normalize h) (normalize h').

(* begin hide *)
Lemma if_then_a_else_a {A : Type} : forall (a : bool) (b : A), (if a then b else b) = b.
Proof.
  now destruct a.
Qed.
(* end hide *)

Lemma eq_remove_one_add_one : forall a, normal a -> a -- ω^zero +: zero <? a = true -> a -- ω^zero +: zero + ω^zero +: zero = a.
Proof.
  induction a ; intros N H.
  - now rewrite ltb_cantor_irefl in H.
  - simpl.
    repeat rewrite ltb_cantor_zero_r.
    rewrite if_then_a_else_a.
    rewrite remove_cantor_zero_r.
    destruct a1.
    + inversion_clear N.
      apply leb_cantor_zero_r in H2.
      destruct (degree_zero_scale a2) as [n H3] ; try assumption.
      subst.
      destruct n.
      * reflexivity.
      * simpl.
        now rewrite add_cantor_zero_r.
    + simpl.
      apply eq_cantor_inj ; split ; try reflexivity.
      simpl in H.
      repeat rewrite ltb_cantor_irefl in H.
      repeat rewrite eqb_cantor_refl in H.
      simpl in H.
      apply IHa2 ; try assumption.
      now inversion N.
Qed.

Lemma round_lt : forall h h', round h h' = true -> normalize h' <? normalize h = true.
Proof.
  unfold round.
  intro h.
  pose proof (normalize_normal h).
  induction (normalize h) ; intro h' ; pose proof (normalize_normal h') ; induction (normalize h') ; intros.
  - easy.
  - easy.
  - destruct c1 ; destruct c2 ; easy.
  - simpl in H1.
    destruct c2 as []eqn:? ; rewrite <- Heqc in * ; destruct c1 as []eqn:? ; rewrite <- Heqc0 in *.
    + rewrite Heqc, Heqc0 in H1.
      destruct (zero =? c3)eqn:? ; [| destruct (zero =? c4) ].
      * easy.
      * easy.
      * now rewrite ltb_cantor_zero_r in H1.
    + destruct (c1 =? c3)eqn:? ; [| destruct (c2 =? c4) as []eqn:? ].
      * apply eqb_cantor_eq in Heqb.
        now subst.
      * simpl.
        cut (c3 <? c1 = true).
        intro.
        now rewrite H2.
        inversion_clear H0.
        apply normal_normalize in H2.
        rewrite <- H2.
        apply IHc1.
        now inversion H.
        now rewrite H2.
      * destruct (c1 -- (ω^c2 +: c2) <? c1)eqn:? ; destruct (c3 =? c1 -- (ω^c2 +: c2))eqn:?.
        -- cut (c3 <? c1 = true).
           intro.
           simpl.
           now rewrite H2.
           apply eqb_cantor_eq in Heqb2.
           now rewrite Heqb2.
        -- destruct (c2 <? ω^c3 +: c4)eqn:? ; try easy.
           simpl in H1.
           rewrite Heqc in *.
           easy.
        -- easy.
        -- easy.
    + destruct (c1 =? c3)eqn:? ; [| destruct (c2 =? c4) as []eqn:? ].
      * apply eqb_cantor_eq in Heqb.
        rewrite Heqb.
        cut (c4 <? c2 = true).
        intro.
        simpl.
        now rewrite ltb_cantor_irefl, eqb_cantor_refl, H2.
        inversion_clear H0.
        apply normal_normalize in H3.
        rewrite <- H3.
        apply IHc2.
        now inversion H.
        now rewrite H3.
      * simpl.
        cut (c3 <? c1 = true).
        intro.
        now rewrite H2.
        inversion_clear H0.
        apply normal_normalize in H2.
        rewrite <- H2.
        apply IHc1.
        now inversion H.
        now rewrite H2.
      * destruct (c1 -- (ω^c1 +: c1) <? c1)eqn:? ; [ rewrite Heqc0 in Heqb1 ; now rewrite ltb_cantor_irefl in Heqb1 | destruct (c3 =? c1 -- (ω^c1 +: c1))eqn:? ] ; try easy. 
    + destruct (c1 =? c3)eqn:? ; [| destruct (c2 =? c4) as []eqn:? ].
      * apply eqb_cantor_eq in Heqb.
        rewrite Heqb.
        cut (c4 <? c2 = true).
        intro.
        simpl.
        now rewrite ltb_cantor_irefl, eqb_cantor_refl, H2.
        inversion_clear H0.
        apply normal_normalize in H3.
        rewrite <- H3.
        apply IHc2.
        now inversion H.
        now rewrite H3.
      * simpl.
        cut (c3 <? c1 = true).
        intro.
        now rewrite H2.
        inversion_clear H0.
        apply normal_normalize in H2.
        rewrite <- H2.
        apply IHc1.
        now inversion H.
        now rewrite H2.
      * destruct (c1 -- (ω^zero +: zero) <? c1)eqn:? ; destruct (c3 =? c1 -- (ω^zero +: zero))eqn:? ; try easy.
        -- cut (c3 <? c1 = true).
           intro.
           simpl.
           now rewrite H2.
           apply eqb_cantor_eq in Heqb2.
           now rewrite Heqb2.
        -- destruct (c2 <? ω^c3 +: c4)eqn:? ; try easy.
           simpl in H1.
           apply eqb_cantor_eq in H1.
           inversion_clear H.
           unfold le_cantor, leb_cantor in H4.
           destruct (degree c2 <? c1)eqn:?.
           ++ simpl.
              cut (c3 <? c1 = true).
                intro H5.
                now rewrite H5.
              apply (sub_exp_leb c2 (c1 -- ω^zero +: zero)) in H3 as H5.
              apply leb_ltb_cantor_trans with (degree c2) ; try assumption.
              assert (c3 = degree (sub_exp c2 (c1 -- ω^zero +: zero))) by (now rewrite H1).
              rewrite H.
              now apply leb_cantor_degree_mono, sub_exp_leb.
           ++ simpl in H4.
              apply eqb_cantor_eq in H4.
              destruct (dominant_part (sub_exp c2 (c1 -- ω^zero +: zero))) as [ [a [n [? [? []]]]] | [n ?] ].
              ** now apply sub_exp_normal.
              ** assert (degree (sub_exp c2 (c1 -- ω^ zero +: zero)) = c1).
                   destruct c2.
                   simpl.
                   now rewrite <- H4.
                   simpl.
                   simpl in H4.
                   rewrite H4 in *.
                   apply ltb_cantor_st in Heqb1.
                   rewrite eqb_cantor_sym in Heqb1.
                   now rewrite Heqb1.
                 rewrite H8 in H5, H6.
                 rewrite <- H6 in H1.
                 inversion_clear H0.
                 destruct (dominant_part (ω^c3 +: sub_exp c4 (c1 -- ω^zero +: zero))) as [ [b [m [? [? []]]]] | [m ?] ] ; try assumption.
                 --- constructor ; try assumption.
                     +++ now apply sub_exp_normal.
                     +++ apply leb_cantor_trans with (degree c4) ; try assumption.
                         now apply leb_cantor_degree_mono, sub_exp_leb.
                 --- simpl in H12, H13.
                     rewrite <- H13 in *.
                     assert (n = m).
                       apply dominant_part_uniq with (sub_exp c2 (c1 -- ω^ zero +: zero)).
                       now apply sub_exp_normal.
                       exists a.
                       repeat split ; try assumption.
                       now rewrite H8.
                       now rewrite H8.
                       exists b.
                       rewrite <- H6.
                       rewrite H1.
                       repeat split ; try assumption.
                       now rewrite H13.
                       now rewrite H13.
                     rewrite <- H15 in *.
                     destruct n.
                     +++ easy.
                     +++ apply neqb_cantor_neq in Heqb.
                         elim Heqb.
                         simpl scale in H1.
                         assert (degree (merge_cantor (ω^c1 +: scale c1 n) a) = degree (merge_cantor (ω^c3 +: scale c3 n) b)) by (now rewrite H1).
                         repeat rewrite merge_cantor_max_degree in H16.
                         simpl in H16.
                         unfold max_cantor in H16.
                         apply ltb_cantor_antisym in H5.
                         apply ltb_cantor_antisym in H12.
                         destruct H5 as [_ H5].
                         destruct H12 as [_ H12].
                         now rewrite H5, H12 in H16.
                 --- rewrite H0 in H1.
                     assert (zero = c1).
                       rewrite H6 in H1.
                       rewrite H1 in H8.
                       now destruct m.
                     rewrite <- H12 in H5.
                     now rewrite ltb_cantor_zero_r in H5.
              ** rewrite H in *.
                 destruct n ; try easy.
                 simpl in H1.
                 apply eq_cantor_inj in H1.
                 destruct H1.
                 rewrite <- H1 in *.
                 apply neqb_cantor_neq in Heqb.
                 apply ltb_cantor_zero_l in Heqb.
                 rewrite <- H4 in H, Heqb1.
                 apply ltb_cantor_antisym in Heqb1.
                 destruct Heqb1 as [Heqb1 _].
                 apply sub_exp_degree_leb_cantor in Heqb1.
                 rewrite H in Heqb1.
                 simpl in Heqb1.
                 rewrite <- Heqb1 in H4.
                 now rewrite H4, ltb_cantor_irefl in Heqb.
Qed.

(*
Lemma le_sub_exp : forall a b, normal a -> normal b -> sub_exp a b <= b.
Proof.
  induction b ; intros.
  easy.
  simpl.
  destruct (eqb_cantor b1 a).
  inversion_clear H0.
  assert (le_cantor b2 (ω^b1 +: b2)).
  destruct b2.
  easy.
  unfold le_cantor.
  unfold leb_cantor.
  simpl.
  simpl in H3.
  unfold le_cantor in H3.
  unfold leb_cantor in H3.
  destruct (ltb_cantor b2_1 b1).
  easy.
  destruct (eqb_cantor b2_1 b1).
  simpl.
  rewrite ltb_cantor_cons_pow_normal.
  easy.
  now inversion H2.
  now apply leb_cantor_refl.
  now inversion H2.
  easy.
  apply IHb2 in H.
  now apply leb_cantor_trans with b2.
  assumption.
  unfold le_cantor.
  unfold leb_cantor.
  simpl.
  rewrite ltb_cantor_irefl.
  rewrite eqb_cantor_refl.
  apply IHb2.
  assumption.
  now inversion H0.
Qed.
*)

Definition adequate (f : Hydra -> CantorNormalOrd) := forall h h', round h h' = true -> lt_cantorN (f h') (f h).

(** L'ordinal associé à une hydre est sa normalisation *)
Definition f (h : Hydra) := normalizeN h.

(** [f] est adéquate *)
Lemma normalizeN_adequate : adequate f.
Proof.
  unfold adequate.
  unfold f.
  intros.
  apply round_lt in H.
  unfold lt_cantorN, f, normalizeN in H.
  pose proof (normalize_normal h).
  pose proof (normalize_normal h').
  now unfold lt_cantorN, normalizeN.
Qed.

(* begin hide *)
Lemma round_head : forall h, normal h -> round' (h + ω^zero +: zero) h = true.
Proof.
  induction h ; intro N.
  - reflexivity.
  - simpl.
    rewrite ltb_cantor_zero_r, add_cantor_zero_r.
    destruct h1 as []eqn:?.
    + inversion_clear N.
      unfold le_cantor in H1.
      apply leb_cantor_zero_r in H1.
      apply degree_zero_scale in H1 ; try assumption.
      destruct H1 as [n H1].
      assert (ω^ zero +: h2 = scale zero (S n)) by (now rewrite H1).
      rewrite H2.
      clear H2 H1 H0 H IHh2 IHh1 Heqc h1 h2.
      remember (S n) as m.
      clear Heqm n.
      induction m.
      * reflexivity.
      * unfold round' at 1.
        fold round'.
        simpl scale at 1.
        cbv iota beta.
        simpl scale at 1.
        cbv iota beta.
        now rewrite eqb_cantor_refl.
    + rewrite <- Heqc in *.
      simpl.
      rewrite Heqc at 1.
      rewrite eqb_cantor_refl.
      apply IHh2.
      now inversion N.
Qed.

Fixpoint least_degree a :=
  match a with
  | zero => zero
  | ω^a +: zero => a
  | ω^_ +: a => least_degree a
  end.

Lemma least_degree_normal : forall a, normal a -> normal (least_degree a).
Proof.
  induction a ; intro N.
  - constructor.
  - destruct a2.
    + now inversion N.
    + apply IHa2.
      now inversion N.
Qed.

Lemma leb_cantor_least_degree_degree : forall a, normal a -> least_degree a <=? degree a = true.
Proof.
  induction a ; intro N.
  - now rewrite leb_cantor_refl.
  - simpl.
    destruct a2 as []eqn:?.
    + now rewrite leb_cantor_refl.
    + rewrite <- Heqc in *.
      inversion_clear N.
      apply leb_cantor_trans with (degree a2) ; try assumption.
      now apply IHa2.
Qed.

Lemma degree_least_degree : forall a, normal a -> degree a = least_degree a -> exists n, a = scale (degree a) n.
Proof.
  induction a ; intros N H.
  - now exists 0.
  - simpl least_degree in H.
    destruct a2 as []eqn:?.
    + now exists 1.
    + rewrite <- Heqc in *.
      simpl in H.
      inversion_clear N.
      apply leb_cantor_least_degree_degree in H1 as H3.
      assert (a1 = degree a2).
        apply leb_normal_antisym ; try assumption.
        now apply normal_degree.
        now rewrite H.
      apply IHa2 in H1.
      * destruct H1 as [n H1].
        exists (S n).
        simpl.
        now rewrite H1, H4.
      * now subst.
Qed.

Lemma least_part : forall a, normal a -> (exists b n, normal b /\ least_degree a <? least_degree b = true /\ merge_cantor b (scale (least_degree a) n) = a /\ n <> 0) \/ (exists n, a = scale (least_degree a) n).
Proof.
  induction a ; intro N.
  - right.
    now exists 0.
  - inversion_clear N.
    apply IHa2 in H0.
    destruct H0 as [ [b [n [Nb [? []]]]] | n ?].
    + left.
      exists (ω^a1 +: b), n.
      repeat split.
      * constructor ; try assumption.
        cut (degree b = degree a2).
          intro H4.
          now rewrite H4.
        rewrite <- H2.
        rewrite merge_cantor_max_degree.
        destruct n ; try easy.
        simpl.
        unfold max_cantor.
        assert (least_degree a2 <=? degree b = true).
          apply ltb_cantor_leb_incl, ltb_leb_cantor_trans with (least_degree b) ; try assumption.
          now apply leb_cantor_least_degree_degree.
        now rewrite ltb_cantor_negb_leb, H4.
      * 
Abort.

Lemma eq_remove_least_degree_add_least_degree : forall a, normal a -> a <> zero -> a -- ω^(least_degree a) +: zero + ω^(least_degree a) +: zero = a.
Proof.
  induction a ; intros N H.
  - contradiction.
  - simpl least_degree.
    destruct a2 as []eqn:?.
    + simpl.
      now rewrite ltb_cantor_irefl, eqb_cantor_refl, add_cantor_zero_l.
    + rewrite <- Heqc in *.
      simpl.
      inversion_clear N.
      destruct (a1 <? least_degree a2)eqn:? ; [| destruct (a1 =? least_degree a2)eqn:? ].
      * assert (a1 <? a1 = true).
          apply ltb_leb_cantor_trans with (least_degree a2) ; try assumption.
          apply leb_cantor_trans with (degree a2) ; try assumption.
          now apply leb_cantor_least_degree_degree.
        now rewrite ltb_cantor_irefl in H3.
      * rewrite ltb_cantor_zero_r.
        apply eqb_cantor_eq in Heqb0.
        rewrite <- Heqb0, ltb_cantor_irefl.
        rewrite remove_cantor_zero_r.
        rewrite Heqb0 in H2.
        assert (degree a2 = least_degree a2).
          apply leb_normal_antisym.
          now apply normal_degree.
          now apply least_degree_normal.
          assumption.
          now apply leb_cantor_least_degree_degree.
        apply degree_least_degree in H3 as H4 ; try assumption.
        destruct H4 as [n H4].
        rewrite H3 in H4.
        rewrite <- Heqb0 in H4.
        rewrite H4.
        destruct n.
        -- reflexivity.
        -- simpl.
           now rewrite ltb_cantor_irefl, add_cantor_zero_r.
      * destruct (ltb_cantor_trichotomy a1 (least_degree a2)) as [H3|[H3|H3]] ; try now rewrite H3 in *.
        rewrite H3.
        simpl.
        rewrite Heqb, H3.
        apply eq_cantor_inj ; split ; try reflexivity.
        apply IHa2 ; now subst.
Qed.

Lemma leb_cantor_cases : forall a a' b b', ω^a +: a' <=? ω^b +: b' = orb (andb (a <=? b) (a' <=? b')) (a <? b).
Proof.
  intros.
  unfold leb_cantor.
  simpl.
  destruct (a <? b) ; [| destruct (a =? b) ].
  - simpl.
    now destruct (a' <? b') ; destruct (a' =? b').
  - simpl.
    now destruct (a' <? b') ; destruct (a' =? b').
  - reflexivity.
Qed.

Lemma ltb_cantor_add_one : forall a, normal a -> a <? a + ω^zero +: zero = true.
Proof.
  induction a ; intro N.
  - easy.
  - simpl.
    rewrite ltb_cantor_zero_r.
    destruct a1 as []eqn:?.
    + rewrite ltb_cantor_irefl, eqb_cantor_refl, add_cantor_zero_r.
      apply ltb_cantor_cons_pow_normal.
      now inversion N.
      now rewrite leb_cantor_refl.
      now inversion N.
    + rewrite <- Heqc in *.
      rewrite ltb_cantor_irefl, eqb_cantor_refl.
      apply IHa2.
      now inversion N.
Qed.

Lemma cantor_set : forall a b, a <> zero -> b <> zero -> normal a -> normal b -> (forall c, normal c -> degree c <? a = true -> c <=? b = true) -> ω^a +: zero <=? b = true.
Proof.
  induction a ; intros b H H' N Nb H0.
  - contradiction.
  - destruct b as [| b1 b2 ]eqn:?.
    + contradiction.
    + simpl.
      rewrite leb_cantor_cases.
      rewrite leb_cantor_zero_l.
      unfold leb_cantor.
      destruct (ltb_cantor_trichotomy (ω^a1 +: a2) b1) as [H1|[H1|H1]] ; try rewrite H1.
      * easy.
      * now destruct (ω^ a1 +: a2 <? b1).
      * pose proof (H0 (ω^b1 +: b2 + ω^zero +: zero)).
        assert (ω^ b1 +: b2 + ω^ zero +: zero <=? ω^ b1 +: b2 = true).
          apply H2.
          apply add_cantor_normal ; try assumption.
          repeat constructor.
          rewrite add_cantor_max_degree.
          simpl.
          unfold max_cantor.
          now rewrite ltb_cantor_zero_r.
        pose proof (ltb_cantor_negb_leb (ω^ b1 +: b2) (ω^ b1 +: b2 + ω^ zero +: zero)).
        rewrite H3 in H4.
        rewrite ltb_cantor_add_one in H4 ; try assumption.
        easy.
Qed.

Lemma leb_cantor_add_r : forall a b c, normal a -> normal b -> normal c -> b <=? c = true -> a + b <=? a + c = true.
Proof.
  induction a ; intros b c N Nb Nc H.
  - now rewrite add_cantor_zero_l.
  - simpl.
    destruct b ; destruct c.
    + now rewrite leb_cantor_refl.
    + destruct (a1 <? c1)eqn:? ; [| destruct (c1 <? a1)eqn:? ].
      * rewrite leb_cantor_cases, Heqb.
        now destruct (andb _ _).
      * rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        assert (a2 <=? a2 + ω^ c1 +: c2 = true).
          rewrite <- add_cantor_zero_r at 1.
          apply IHa2 ; try now inversion N.
        now rewrite H0.
      * rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        destruct (ltb_cantor_trichotomy a1 c1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        assert (a2 <=? ω^ c1 +: (a2 + c2) = true).
          inversion_clear N.
          inversion_clear Nc.
          apply ltb_cantor_leb_incl.
          apply ltb_cantor_cons_pow_normal.
          now apply add_cantor_normal.
          rewrite <- add_cantor_zero_r at 1.
          rewrite IHa2 ; try assumption.
          now rewrite leb_cantor_zero_l.
          rewrite add_cantor_max_degree.
          unfold max_cantor.
          now destruct (degree a2 <? degree c2).
        now rewrite H0.
    + unfold leb_cantor in H.
      unfold eqb_cantor in H.
      now rewrite ltb_cantor_zero_r in H.
    + destruct (a1 <? b1)eqn:? ; [ destruct (a1 <? c1)eqn:? ; destruct (c1 <? a1)eqn:? | destruct (b1 <? a1)eqn:? ; destruct (a1 <? c1)eqn:? ; destruct (c1 <? a1)eqn:? ] ; try easy.
      * rewrite leb_cantor_cases.
        rewrite ltb_cantor_negb_leb in Heqb.
        destruct (b1 <=? a1) ; try easy.
        simpl.
        apply leb_ltb_cantor_trans with c1 ; try assumption.
        rewrite leb_cantor_cases in H.
        destruct (b1 <=? c1)eqn:? ; try easy.
        simpl in H.
        apply ltb_cantor_leb_incl in H.
        now rewrite H in Heqb2.
      * destruct (ltb_cantor_trichotomy a1 c1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        rewrite leb_cantor_cases in H.
        rewrite ltb_cantor_negb_leb in Heqb.
        destruct (b1 <=? c1)eqn:? ; try easy.
        simpl in H.
        apply ltb_cantor_leb_incl in H.
        now rewrite H in Heqb2.
      * apply ltb_cantor_antisym in Heqb1.
        destruct Heqb1.
        now rewrite H1 in Heqb2.
      * rewrite leb_cantor_cases, Heqb1.
        now destruct (andb _ _).
      * rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        rewrite IHa2 ; now inversion N.
      * rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        destruct (ltb_cantor_trichotomy a1 c1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        assert (a2 + ω^ b1 +: b2 <=? ω^ c1 +: (a2 + c2) = true).
          admit.
        now rewrite H0.
      * apply ltb_cantor_antisym in Heqb1.
        destruct Heqb1.
        now rewrite H1 in Heqb2.
      * rewrite leb_cantor_cases, Heqb1.
        now destruct (andb _ _).
      * rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        destruct (ltb_cantor_trichotomy a1 b1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        assert (ω^ b1 +: (a2 + b2) <=? a2 + ω^ c1 +: c2 = true).
          admit.
        now rewrite H0.
      * destruct (ltb_cantor_trichotomy a1 b1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        destruct (ltb_cantor_trichotomy b1 c1) as [H0|[H0|H0]] ; try now rewrite H0 in *.
        apply eqb_cantor_eq in H0.
        subst.
        simpl.
        rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        rewrite leb_cantor_cases, leb_cantor_refl.
        simpl.
        rewrite IHa2.
        -- reflexivity.
        -- now inversion N.
        -- now inversion Nb.
        -- now inversion Nc.
        -- rewrite leb_cantor_cases in H.
           rewrite leb_cantor_refl, ltb_cantor_irefl in H.
           simpl in H.
           now destruct (b2 <=? c2).
Admitted.

Lemma cantor_set_least_degree : forall a b, least_degree a <> zero -> b <> zero -> normal a -> normal b -> (forall c, normal c -> degree c <? least_degree a = true -> c <=? b = true) -> a <=? a -- ω^least_degree a +: zero + b = true.
Proof.
  intros.
  rewrite <- eq_remove_least_degree_add_least_degree at 1.
  apply leb_cantor_add_r.
  - now apply remove_cantor_normal.
  - now apply exp_normal, least_degree_normal.
  - assumption.
  - apply cantor_set ; try assumption.
    now apply least_degree_normal.
  - assumption.
  - admit.
Admitted.

Lemma merge_cantor_ltb_least_degree : forall a b, normal a -> normal b -> degree b <? least_degree a = true -> merge_cantor (a -- least_degree a) b <? a = true.
Admitted.

Lemma remove_cantor_one_least_degree : forall a, a <> zero -> normal a -> a -- ω^zero +: zero <? a = false -> least_degree a <> zero.
Proof.
  induction a ; intros H N H0.
  - contradiction.
  - simpl.
    destruct a2 as []eqn:?.
    + intro.
      now subst.
    + rewrite <- Heqc in *.
      apply IHa2.
      * now rewrite Heqc.
      * now inversion N.
      * simpl in H0.
        repeat rewrite ltb_cantor_zero_r in H0.
        rewrite if_then_a_else_a in H0.
        destruct a1 as []eqn:?.
        -- rewrite remove_cantor_zero_r in H0.
           assert (a2 <? ω^zero +: a2 = true).
             apply ltb_cantor_cons_pow_normal.
             now inversion N.
             now rewrite leb_cantor_refl.
             now inversion N.
           now rewrite H0 in H1.
        -- rewrite <- Heqc0 in *.
           simpl in H0.
           now rewrite ltb_cantor_irefl, eqb_cantor_refl in H0.
Qed.

Lemma least_degree_zero : forall a, normal a -> least_degree a = zero -> a = zero \/ a -- ω^zero +: zero <? a = true.
Proof.
  induction a ; intros N H.
  - now left.
  - right.
    simpl in H.
    destruct a2 as []eqn:?.
    + now subst.
    + rewrite <- Heqc in *.
      simpl.
      repeat rewrite ltb_cantor_zero_r.
      rewrite if_then_a_else_a.
      destruct a1 as []eqn:?.
      * admit.
      * rewrite <- Heqc0 in *.
        simpl.
        rewrite ltb_cantor_irefl, eqb_cantor_refl.
        inversion_clear N.
        apply IHa2 in H1 ; try assumption.
        destruct H1.
        -- now subst.
        -- assumption.
Admitted.

Lemma adequate_increasing : forall f, adequate f -> forall a, le_cantorN a (f (normal_value a)).
Proof.
  intros f H a.
  induction (least_degree (normal_value a))eqn:?.
  - destruct (least_degree_zero (normal_value a)) as [Heqb|Heqb] ; try apply normality ; try assumption.
    + destruct a as [a Na].
      simpl in Heqb.
      subst.
      simpl.
      destruct (f zero).
      unfold le_cantorN, le_cantor.
      now rewrite leb_cantor_zero_l.
    + apply transfinite_ind.
      intros [b Nb] H0.
      destruct a as [a Na].
      apply eq_remove_one_add_one in Na as H1 ; try assumption.
      pose proof (H a (a -- ω^zero +: zero)).
      simpl normal_value.
      destruct (f a)eqn:?.
      destruct (f (a -- ω^ zero +: zero))eqn:?.
      unfold le_cantorN, le_cantor.
      unfold lt_cantorN, lt_cantor in H2.
      admit.
  - 
Abort. (*
      rewrite <- H2.
      apply ltb_leb_add_normal ; try assumption.
      now apply remove_cantor_normal.
      apply leb_ltb_cantor_trans with a1 ; try assumption.
      assert (normal (a -- ω^zero +: zero)) by (apply remove_cantor_normal ; assumption).
      cut (le_cantorN (Normal (a -- ω^zero +: zero) H3) (f (a -- ω^zero +: zero))).
        intro.
        now rewrite Heqc1 in H4.
      now apply H0.
      cut (lt_cantorN (Normal a1 n0) (Normal a0 n)).
        easy.
      rewrite <- Heqc0, <- Heqc1.
      apply H.
      rewrite <- H1 at 1.
      unfold round.
      rewrite normal_normalize.
      rewrite normal_normalize.
      apply round_head.
      now apply remove_cantor_normal.
      now apply remove_cantor_normal.
      apply add_cantor_normal ; try repeat constructor.
      now apply remove_cantor_normal.
*)

Lemma adequate_increasing : forall f, adequate f -> forall a, le_cantorN a (f (normal_value a)).
Proof.
  intros f H.
  apply transfinite_ind.
  intros.
  destruct a as [a Na].
  destruct (a -- ω^zero +: zero <? a)eqn:?.
  - apply eq_remove_one_add_one in Na as H1 ; try assumption.
    pose proof (H a (a -- ω^zero +: zero)).
    simpl normal_value.
    destruct (f a)eqn:?.
    destruct (f (a -- ω^ zero +: zero))eqn:?.
    unfold le_cantorN, le_cantor.
    unfold lt_cantorN, lt_cantor in H2.
    rewrite <- H1.
    apply ltb_leb_add_normal ; try assumption.
    now apply remove_cantor_normal.
    apply leb_ltb_cantor_trans with a1 ; try assumption.
    assert (normal (a -- ω^zero +: zero)) by (apply remove_cantor_normal ; assumption).
    cut (le_cantorN (Normal (a -- ω^zero +: zero) H3) (f (a -- ω^zero +: zero))).
      intro.
      now rewrite Heqc0 in H4.
    now apply H0.
    cut (lt_cantorN (Normal a1 n0) (Normal a0 n)).
      easy.
    rewrite <- Heqc, <- Heqc0.
    apply H.
    rewrite <- H1 at 1.
    unfold round.
    rewrite normal_normalize.
    rewrite normal_normalize.
    apply round_head.
    now apply remove_cantor_normal.
    now apply remove_cantor_normal.
    apply add_cantor_normal ; try repeat constructor.
    now apply remove_cantor_normal.
  - simpl normal_value.
    assert (forall b Nb, b <? a = true -> le_cantorN (Normal b Nb) (f b)).
      intros b Nb H1.
      assert (b = normal_value (Normal b Nb)) by reflexivity.
      rewrite H2 at 2.
      now apply H0.
    remember Na as N.
    destruct a as []eqn:?.
    + unfold le_cantorN, le_cantor.
      destruct (f _).
      now rewrite leb_cantor_zero_l.
    + clear HeqN.
      rewrite <- Heqc in Na, Heqb.
      assert (a <> zero) by (now subst).
      apply remove_cantor_one_least_degree in H1 as H2 ; try assumption.
      destruct (f _) as [b Nb]eqn:?.
      unfold le_cantorN, le_cantor.
      rewrite <- Heqc.
      simpl in Heqc0.
      rewrite <- Heqc in Heqc0.
      
      assert (forall c, normal c -> degree c <? least_degree a = true -> c <=? b -- (a -- least_degree a) = true).
        intros c Nc H3.
        assert (normal (a -- least_degree a + c)) as Nac.
          apply add_cantor_normal ; try assumption.
          apply remove_cantor_normal ; try assumption.
        pose proof (H0 (Normal (a -- least_degree a + c) Nac)).
        unfold lt_cantorN, lt_cantor in H4.
        rewrite <- Heqc in H4.
Abort.

(** Théorème de Paris-Kirby :
    ∀[f] adéquate, sup#<sub>#[h : Hydra]#</sub># [f]([h]) = ε#<sub>0</sub># *)
Theorem adequate_ε0 : forall f, adequate f -> forall a, exists b, lt_cantorN a (f b).
Proof.
  intros f H a.
  destruct a as [a Na].
  exists (a + ω^zero +: zero).
  destruct (f (a + ω^zero +: zero)) as [b Nb]eqn:?.
  unfold lt_cantorN, lt_cantor.
  apply ltb_leb_add_normal ; try assumption.
  pose proof (adequate_increasing f H (Normal (a + ω^zero +: zero) (add_cantor_normal _ _ Na one_normal))).
  simpl in H0.
  now rewrite Heqc in H0.
Qed.

End CantorOrd.
(* end hide *)
