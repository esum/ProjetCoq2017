(* begin hide *)
Require Import Omega.
(* end hide *)

Inductive bin : Set :=
  | Zero : bin
  | Double : bin -> bin
  | DoubleOne : bin -> bin.
(**  Un entier positif peut avoir plusieurs représentation par un objet du type [bin],
     par exemple [0] est représentable par [Zero], [Double Zero], [Double (Double Zero)], ... *)
(* begin hide *)
(* end hide *)

(** Successeur d'un entier de type [bin] *)
Fixpoint binS b :=
  match b with
  | Zero => DoubleOne Zero
  | Double d => DoubleOne d
  | DoubleOne d => Double (binS d)
  end.
(** On remarque :
   - [0 + 1 = 2 * 0 + 1]
   - [2n + 1 = 2n + 1]
   - [2n + 1 + 1 = 2n + 2 = 2(n + 1)] *)
(* begin hide *)
(* end hide *)

(** Conversion d'un entier du type [nat] vers le type [bin] *)
Fixpoint f n :=
  match n with
  | 0 => Zero
  | S m => binS (f m)
  end.

(** Conversion d'un entier du type [bin] vers le type [nat] *)
Fixpoint g b :=
  match b with
  | Zero => 0
  | Double d => g d + g d
  | DoubleOne d => S (g d + g d)
  end.

(** Normalise un entier de type [bin] en supprimant les [Double Zero] terminaux *)
Fixpoint h b :=
  match b with
  | Zero => Zero
  | Double d => 
    match h d with
    | Zero => Zero
    | d' => Double d'
    end
  | DoubleOne d => DoubleOne (h d)
  end.


(** Correspondance entre [binS] et [S] *)
Lemma g_succ : forall b, g (binS b) = S (g b).
Proof.
  induction b.
  - easy.
  - easy.
  - simpl.
    rewrite IHb.
    omega.
Qed.

(* begin show *)
(** [g] ∘ [f] est la fonction identité sur les entiers de type [nat] *)
Theorem g_f_bij : forall n, g (f n) = n.
Proof.
  induction n.
  - easy.
  - simpl.
    destruct (f n) ; simpl ; rewrite <- IHn.
    + easy.
    + easy.
    + simpl.
      rewrite g_succ.
      omega.
Qed.
(* end show *)

(* begin show *)
Theorem h_invar : forall b, g b = g (h b).
Proof.
  induction b.
  - easy.
  - simpl.
    now destruct (h b) ; rewrite IHb.
  - simpl.
    now rewrite IHb.
Qed.
(* end show *)

(** Correspondance entre [* 2] et [Double] *)
Lemma f_add : forall n, n <> 0 -> f (n + n) = Double (f n).
Proof.
  induction n.
  - easy.
  - intro.
    simpl.
    rewrite <- Nat.add_succ_comm.
    simpl.
    destruct n.
    + now simpl.
    + now rewrite IHn.
Qed.

Lemma g_h_zero : forall b, g (h b) = 0 -> h b = Zero.
Proof.
  induction b ; intro H.
  - easy.
  - simpl in H.
    destruct (h b) as []eqn:Heqb0.
    + simpl.
      now rewrite Heqb0.
    + rewrite <- Heqb0 in H.
      simpl in H.
      apply Nat.eq_add_0 in H.
      destruct H as [H _].
      rewrite Heqb0 in H.
      apply IHb in H.
      rewrite H in Heqb0.
      simpl.
      now rewrite Heqb0.
    + now simpl in H.
  - now simpl in H.
Qed.

(* begin show *)
(** [f] ∘ [g] est la fonction identité sur les entiers de type [bin] normalisés *)
Theorem f_g_h_bij : forall b, f (g (h b)) = h b.
Proof.
  induction b.
  - reflexivity.
  - simpl.
    destruct (h b) as []eqn:Heqb0.
    + reflexivity.
    + simpl.
      destruct (g b0 + g b0) as []eqn:Heqn.
      * simpl.
        simpl in IHb.
        now rewrite Heqn in IHb.
      * rewrite f_add ; try easy.
        rewrite <- Heqn.
        assert (g b0 + g b0 = g (Double b0)) as H by easy.
        now rewrite H, IHb.
    + assert (g (Double (DoubleOne b0)) = g (DoubleOne b0) + g (DoubleOne b0)) as H by easy.
      rewrite H.
      clear H.
      rewrite f_add ; try easy.
      now rewrite IHb.
  - simpl.
    destruct (g (h b)).
    + now rewrite <- IHb.
    + rewrite f_add ; try easy.
      now rewrite <- IHb.
Qed.
(* end show *)

Lemma h_eq_f_g : forall b, h b = f (g b).
Proof.
  induction b.
  - reflexivity.
  - simpl.
    destruct (g b).
    + simpl in IHb.
      now rewrite IHb.
    + rewrite IHb.
      rewrite f_add ; try easy.
      destruct (f (S n)) as []eqn:?.
      * simpl in Heqb0.
        now destruct (f n).
      * reflexivity.
      * reflexivity.
  - simpl.
    destruct (g b) ; rewrite IHb.
    + easy.
    + now rewrite f_add.
Qed.
